package com.deutzapp.azularc.data;

import java.io.Serializable;
import java.io.SerializablePermission;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceProviderResponseList implements Serializable {

    @SerializedName("serviceproviderlist")
    @Expose
    private List<Serviceproviderlist> serviceproviderlist = null;

    public List<Serviceproviderlist> getServiceproviderlist() {
        return serviceproviderlist;
    }

    public void setServiceproviderlist(List<Serviceproviderlist> serviceproviderlist) {
        this.serviceproviderlist = serviceproviderlist;
    }

    @Override
    public String toString() {
        return "ServiceProviderResponseList{" +
                "serviceproviderlist=" + serviceproviderlist +
                '}';
    }
}