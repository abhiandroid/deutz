package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceProviderRequest {
    @SerializedName("serviceprovider_types")
    @Expose
    private String serviceproviderTypes;
    @SerializedName("selecteduser_lat")
    @Expose
    private String selecteduserLat;
    @SerializedName("selecteduser_lon")
    @Expose
    private String selecteduserLon;

    @SerializedName("selected_brand")
    @Expose
    private String selectedBrand;

    public String getServiceproviderTypes() {
        return serviceproviderTypes;
    }

    public void setServiceproviderTypes(String serviceproviderTypes) {
        this.serviceproviderTypes = serviceproviderTypes;
    }

    public String getSelecteduserLat() {
        return selecteduserLat;
    }

    public void setSelecteduserLat(String selecteduserLat) {
        this.selecteduserLat = selecteduserLat;
    }

    public String getSelecteduserLon() {
        return selecteduserLon;
    }

    public void setSelecteduserLon(String selecteduserLon) {
        this.selecteduserLon = selecteduserLon;
    }

    public String getSelectedBrand() {
        return selectedBrand;
    }

    public void setSelectedBrand(String selectedBrand) {
        this.selectedBrand = selectedBrand;
    }

    @Override
    public String toString() {
        return "ServiceProviderRequest{" +
                "serviceproviderTypes='" + serviceproviderTypes + '\'' +
                ", selecteduserLat='" + selecteduserLat + '\'' +
                ", selecteduserLon='" + selecteduserLon + '\'' +
                ", selectedBrand='" + selectedBrand + '\'' +
                '}';
    }

    public ServiceProviderRequest(String serviceproviderTypes, String selecteduserLat, String selecteduserLon) {
        this.serviceproviderTypes = serviceproviderTypes;
        this.selecteduserLat = selecteduserLat;
        this.selecteduserLon = selecteduserLon;
        this.selectedBrand = selectedBrand;
    }

    public ServiceProviderRequest(String serviceproviderTypes, String selecteduserLat, String selecteduserLon,String selectedBrand) {
        this.serviceproviderTypes = serviceproviderTypes;
        this.selecteduserLat = selecteduserLat;
        this.selecteduserLon = selecteduserLon;
        this.selectedBrand=selectedBrand;
    }
}