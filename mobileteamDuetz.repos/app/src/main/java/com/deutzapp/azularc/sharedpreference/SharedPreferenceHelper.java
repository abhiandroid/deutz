package com.deutzapp.azularc.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class SharedPreferenceHelper {

    public static final String DONT_SHOW = "dont_show";
    public static final String FIRST_TIME = "first_time";

    public static final String NO_JOBS = "no_jobs";

    public static final String PERSISTENT_PUSH = "persistent_push";

    public static final String BASE64STRING = "base64string";

    public static final String CLOCK_IN = "clock_in";
    public static final String CLOCK_OUT = "clock_out";

    public static final String REQUIREMENT_VIDEO_STATUS = "requirement_video_status";
    public static final String REQUIREMENT_WAIVER_STATUS = "requirement_waiver_status";

    public static final String PERSISTENT_ID = "persistent_id";
    public static final String PERSISTENT_ID_LIST = "persistent_id_list";

    public static final String CLOCK_IN_TIME = "clock_in_time";
    public static final String CLOCK_IN_TIMESTAMP = "clock_in_timestamp";
    public static final String CLOCK_OUT_TIME_DIFF = "clock_out_time_diff";
    public static final String CLOCK_OUT_TIME = "clock_out_time";



    /* Required Task Keys */
    public static final String WATCHED = "video";
    public static final String eulaKey = "agree";

    public static final String ACCESS_TOKEN = "access_token";
    public static final String TOKEN_SECRET = "token_secret";
    public static final String USER_TYPE = "user_type";

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String LOGOUT = "logout";


    public static final String FULLNAME = "fullName";
    public static final String PHNO = "phone_no";
    public static final String USEREMAIL = "user_email";
    public static final String PREFMETHCONT = "pref_contact";
    public static final String BIRTHDATE = "birth_date";
    public static final String PROFILEPIC = "profile_pic";
    public static final String RATING = "rating";
    public static final String NOOFJOBSCOMPLETED = "no_jobs_completed";
    public static final String WORKERID = "workerid";
    public static final String REPORT_AN_ACCIDENT = "report_accident";

    public static final String COMPANY_NAME = "comapny_name";
    public static final String COMPANY_DESC = "company_desc";
    public static final String COMPANY_PREF_IND = "company_pref_ind";

    public static final String COMPANY_ONSITE_CONTACT_NAME = "company_onsite_contact_name";

    public static final String ONDEMAND_STATUS = "ondemand_status";

    public static final String JOBIDS = "jobids";

//	public static final  String SET_AVB_STRT_TIME="set_avb_strt_time";
//	public static final  String SET_AVB_END_TIME="set_avb_end_time";
//	public static final  String SET_AVB_SEL_DATES="set_avb_sel_dates";


    public static void savePreferences(String key, String value, Context ctx) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreference(Context ctx, String key) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        if (key.equals(CLOCK_IN_TIME))
            return sharedPreferences.getString(key, 0 + "");
        return sharedPreferences.getString(key, "");
    }

    public static void removePreference(Context ctx, String key) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();

    }

    public static void saveBooleanPreferences(String key, boolean value, Context ctx) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBooleanPreferences(Context ctx, String key, boolean value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        return sharedPreferences.getBoolean(key, value);
    }


}
