package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Serviceproviderdetail {

    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("provider_name")
    @Expose
    private String providerName;
    @SerializedName("provider_lat")
    @Expose
    private String providerLat;
    @SerializedName("provider_long")
    @Expose
    private String providerLon;
    @SerializedName("provider_address")
    @Expose
    private String providerAddress;
    @SerializedName("provider_address_details")
    @Expose
    private String providerAddressDetails;
    @SerializedName("provider_type")
    @Expose
    private String providerType;
    @SerializedName("header_image")
    @Expose
    private String headerImage;
    @SerializedName("provider_sitelink")
    @Expose
    private String providerSitelink;
    @SerializedName("provider_mailid")
    @Expose
    private String providerMailid;
    @SerializedName("provider_contactno")
    @Expose
    private String providerContactno;
    @SerializedName("provider_24hourcontact")
    @Expose
    private String provider24hourcontact;
    @SerializedName("brand_names")
    @Expose
    private String brandNames;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderLat() {
        return providerLat;
    }

    public void setProviderLat(String providerLat) {
        this.providerLat = providerLat;
    }

    public String getProviderLon() {
        return providerLon;
    }

    public void setProviderLon(String providerLon) {
        this.providerLon = providerLon;
    }

    public String getProviderAddress() {
        return providerAddress;
    }

    public void setProviderAddress(String providerAddress) {
        this.providerAddress = providerAddress;
    }

    public String getProviderAddressDetails() {
        return providerAddressDetails;
    }

    public void setProviderAddressDetails(String providerAddressDetails) {
        this.providerAddressDetails = providerAddressDetails;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public String getHeaderImage() {
        return headerImage;
    }

    public void setHeaderImage(String headerImage) {
        this.headerImage = headerImage;
    }

    public String getProviderSitelink() {
        return providerSitelink;
    }

    public void setProviderSitelink(String providerSitelink) {
        this.providerSitelink = providerSitelink;
    }

    public String getProviderMailid() {
        return providerMailid;
    }

    public void setProviderMailid(String providerMailid) {
        this.providerMailid = providerMailid;
    }

    public String getProviderContactno() {
        return providerContactno;
    }

    public void setProviderContactno(String providerContactno) {
        this.providerContactno = providerContactno;
    }

    public String getProvider24hourcontact() {
        return provider24hourcontact;
    }

    public void setProvider24hourcontact(String provider24hourcontact) {
        this.provider24hourcontact = provider24hourcontact;
    }

    public String getBrandNames() {
        return brandNames;
    }

    public void setBrandNames(String brandNames) {
        this.brandNames = brandNames;
    }

    @Override
    public String toString() {
        return "Serviceproviderdetail{" +
                "providerId='" + providerId + '\'' +
                ", providerName='" + providerName + '\'' +
                ", providerLat='" + providerLat + '\'' +
                ", providerLon='" + providerLon + '\'' +
                ", providerAddress='" + providerAddress + '\'' +
                ", providerAddressDetails='" + providerAddressDetails + '\'' +
                ", providerType='" + providerType + '\'' +
                ", headerImage='" + headerImage + '\'' +
                ", providerSitelink='" + providerSitelink + '\'' +
                ", providerMailid='" + providerMailid + '\'' +
                ", providerContactno='" + providerContactno + '\'' +
                ", provider24hourcontact='" + provider24hourcontact + '\'' +
                ", brandNames='" + brandNames + '\'' +
                '}';
    }
}