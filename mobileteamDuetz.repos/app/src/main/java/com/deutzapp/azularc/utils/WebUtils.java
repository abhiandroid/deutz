package com.deutzapp.azularc.utils;

/**
 * Created by Azularc on 09-02-2016.
 */


public class WebUtils {

    //public static String MAIN_URL = "http://deutz.azularc.com/"; //--production
    //public static String MAIN_URL = "http://deutz.azularc.com/"; //--staging
    //public static String MAIN_URL = "http://deutz.azularcdev.com/";//--development
    public static String MAIN_URL = "http://deutzstg-mobapi.azularcdev.com/";//--development
    //public static String MAIN_URL = "http://deutz-mobapi.azularcdev.com/";//--production

    //public static String URL = MAIN_URL + "public/";//--staging and production
    public static String URL = MAIN_URL;//--staging and production

    /**
     * Below listed are the list of methods
     */

    public static String BRAND = "getbrands";
    public static String GETSERVICEPROVIDERDETAILS = "getserviceproviderdetails";
    public static String GETSERVICEPROVIDERS = "getserviceproviders";
    public static String GETSERVICEPROVIDERSLIST = "getserviceproviderslist";

}
//public class WebUtils {
//
//    //public static String MAIN_URL = "http://deutz.azularc.com/"; //--production
//    //public static String MAIN_URL = "http://deutz.azularc.com/"; //--staging
//    //public static String MAIN_URL = "http://deutz.azularcdev.com/";//--development
//
//    public static String MAIN_URL="http://deutzstg-mobapi.azularcdev.com/";
//    public static String URL = MAIN_URL + "public/";//--staging and production
//
//    /**
//     * Below listed are the list of methods
//     */
//
//    public static String BRAND = "getbrands";
//    public static String GETSERVICEPROVIDERDETAILS = "getserviceproviderdetails";
//    public static String GETSERVICEPROVIDERS = "getserviceproviders";
//
//    public static String GETSERVICEPROVIDERSLIST = "getserviceproviderslist";
//
//  //  public static String GETSERVICEPROVIDERSLIST = "getserviceproviderslist";
//
//}
