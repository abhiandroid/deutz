package com.deutzapp.azularc.data;

/**
 * Created by NabeelRangari on 3/3/17.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BrandList {

    @SerializedName("brands")
    @Expose
    private List<Brand> brands = null;

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    @Override
    public String toString() {
        return "BrandList{" +
                "brands=" + brands +
                '}';
    }
}