package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prasadlokhande on 12/19/16.
 */

public class AddressDataList {


    @SerializedName("AddressData")
    @Expose
    private List<AddressData> addressDataList = new ArrayList<AddressData>();

    /**
     * @return The availableWorkers
     */
    public List<AddressData> getAddressDataList() {
        return addressDataList;
    }

    /**
     * @param addressDataList The available_workers
     */
    public void setAvailableWorkers(List<AddressData> addressDataList) {
        this.addressDataList = addressDataList;
    }

}