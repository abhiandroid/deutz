package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceProviderDetailsRequest {

    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("selecteduser_lat")
    @Expose
    private String selecteduserLat;
    @SerializedName("selecteduser_lon")
    @Expose
    private String selecteduserLon;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getSelecteduserLat() {
        return selecteduserLat;
    }

    public void setSelecteduserLat(String selecteduserLat) {
        this.selecteduserLat = selecteduserLat;
    }

    public String getSelecteduserLon() {
        return selecteduserLon;
    }

    public void setSelecteduserLon(String selecteduserLon) {
        this.selecteduserLon = selecteduserLon;
    }

    @Override
    public String toString() {
        return "ServiceProviderDetailsRequest{" +
                "providerId='" + providerId + '\'' +
                ", selecteduserLat='" + selecteduserLat + '\'' +
                ", selecteduserLon='" + selecteduserLon + '\'' +
                '}';
    }

    public ServiceProviderDetailsRequest(String providerId, String selecteduserLat, String selecteduserLon) {
        this.providerId = providerId;
        this.selecteduserLat = selecteduserLat;
        this.selecteduserLon = selecteduserLon;
    }
}