package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Serviceproviderlist {

    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("provider_name")
    @Expose
    private String providerName;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("provider_lat")
    @Expose
    private String providerLat;
    @SerializedName("provider_lon")
    @Expose
    private String providerLon;
    @SerializedName("provider_address")
    @Expose
    private String providerAddress;
    @SerializedName("provider_type")
    @Expose
    private String providerType;
    @SerializedName("is24hour_contact")
    @Expose
    private String is24hourContact;
    @SerializedName("distance")
    @Expose
    private Double distance;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getProviderLat() {
        return providerLat;
    }

    public void setProviderLat(String providerLat) {
        this.providerLat = providerLat;
    }

    public String getProviderLon() {
        return providerLon;
    }

    public void setProviderLon(String providerLon) {
        this.providerLon = providerLon;
    }

    public String getProviderAddress() {
        return providerAddress;
    }

    public void setProviderAddress(String providerAddress) {
        this.providerAddress = providerAddress;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public String getIs24hourContact() {
        return is24hourContact;
    }

    public void setIs24hourContact(String is24hourContact) {
        this.is24hourContact = is24hourContact;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Serviceproviderlist{" +
                "providerId='" + providerId + '\'' +
                ", providerName='" + providerName + '\'' +
                ", color='" + color + '\'' +
                ", providerLat='" + providerLat + '\'' +
                ", providerLon='" + providerLon + '\'' +
                ", providerAddress='" + providerAddress + '\'' +
                ", providerType='" + providerType + '\'' +
                ", is24hourContact='" + is24hourContact + '\'' +
                ", distance=" + distance +
                '}';
    }

    public Serviceproviderlist(String providerId, String providerName, String color, String providerLat, String providerLon, String providerAddress, String providerType, String is24hourContact) {
        this.providerId = providerId;
        this.providerName = providerName;
        this.color = color;
        this.providerLat = providerLat;
        this.providerLon = providerLon;
        this.providerAddress = providerAddress;
        this.providerType = providerType;
        this.is24hourContact = is24hourContact;
    }

    public Serviceproviderlist(String providerId, String providerName, String color, String providerLat, String providerLon, String providerAddress, String providerType, String is24hourContact, Double distance) {
        this.providerId = providerId;
        this.providerName = providerName;
        this.color = color;
        this.providerLat = providerLat;
        this.providerLon = providerLon;
        this.providerAddress = providerAddress;
        this.providerType = providerType;
        this.is24hourContact = is24hourContact;
        this.distance = distance;
    }
}