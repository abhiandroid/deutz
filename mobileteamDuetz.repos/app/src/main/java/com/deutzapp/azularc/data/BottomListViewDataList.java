package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prasadlokhande on 12/19/16.
 */

public class BottomListViewDataList {

    @SerializedName("BottomListViewData")
    @Expose
    private List<BottomListViewData> bottomListViewDataList = new ArrayList<BottomListViewData>();

    /**
     * @return The bottomListViewDataList
     */
    public List<BottomListViewData> getBottomListViewDataList() {
        return bottomListViewDataList;
    }

    /**
     * @param bottomListViewDataList The bottomListViewDataList
     */
    public void setBottomListViewDataList(List<BottomListViewData> bottomListViewDataList) {
        this.bottomListViewDataList = bottomListViewDataList;
    }

}