package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by prasadlokhande on 12/19/16.
 */

public class AddressData {

    @SerializedName("_latitude")
    @Expose
    private String latitude;
    @SerializedName("_longitude")
    @Expose
    private String longitude;

    @SerializedName("provider_type")
    @Expose
    private String providerType;

    @SerializedName("provider_name")
    @Expose
    private String providerName;

    /**
     * @return The workerLatitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The worker_latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The workerLongitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The worker_longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getProvider() {
        return providerType;
    }

    public void setProvider(String provider) {
        this.providerType = provider;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    @Override
    public String toString() {
        return "AddressData{" +
                "latitude='" + latitude + '\'' +
                ", provider='" + providerType + '\'' +
                ", longitude='" + longitude + '\'' +
                ", providerName='" + providerName + '\'' +
                '}';
    }
}