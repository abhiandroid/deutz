package com.deutzapp.azularc.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deutzapp.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static final int WEIGHT = 125;
    public static String PROVIDERID = "1";
    public static final String PLEASE_WAIT = "Please wait...";
    public static final String FETCHING_DETAILS = "Fetching details";
    public static final String UPLOADING_DETAILS = "Uploading details";
    public static final String FETCHING_BILLABLE_RATE = "Fetching billable rate...";

    public static void callThisNumber(String phoneNumber, Context ctx) {

        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phoneNumber));
            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            ctx.startActivity(callIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static String LATITUDE = "";
    public static String LONGITUDE = "";


    // public static String LATITUDE = "33.7764609";
    //   public static String LONGITUDE = "-84.5516669";
//
//    public static String LATITUDE = "44.8575461";
//    public static String LONGITUDE = "-93.2479711";

    public static final String AUTHORIZED_DISTRIBUTOR = "Authorized Distributor";
    public static final String FULL_SERVICE_DEALER = "Full-Service Dealer (CC3)";
   // public static final String OEM_DEALER = "OEM Dealer";
    public static final String OEM_FULL_SERVICE_DEALER = "Limited-Service Dealer (CC2)";


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();

        return activeNetworkInfo != null;
    }

    public static String decodeString(String stingvalue)
            throws UnsupportedEncodingException {

        return URLDecoder.decode(stingvalue, "UTF-8");
    }

    public static void sendEmail(Context context, String subject, String body) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        // i.putExtra(Intent.EXTRA_EMAIL, recipients);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, body);
        try {
            context.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static String getFavIds(Context ctx) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        Map<String, ?> keys = sharedPreferences.getAll();
        StringBuffer buff = new StringBuffer();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().toString();
            if (value != null && value.equals("1")) {
                buff.append(key).append(",");
            }
        }
        return buff.toString();
    }

    public static void saveInSdcard(String content) {
        try {
            File myFile = new File("/sdcard/mysdfile.txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(content);
            myOutWriter.close();
            fOut.close();

        } catch (Exception e) {

        }
    }

    public static Date getTodaysDateObj(String format) throws Exception {
        Date date = new Date();
        DateFormat df;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            df = new SimpleDateFormat(format,
                    Locale.UK);
        } else {
            df = new SimpleDateFormat(format,
                    Locale.ENGLISH);
        }
        /*DateFormat df = new SimpleDateFormat(format,
                Locale.ENGLISH);*/
        // Use Madrid's time zone to format the date in
        //df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        date = df.parse(df.format(date));

        return date;
    }

    public static String getTodaysDay(String format) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            return new SimpleDateFormat(format, Locale.UK).format(new Date());
        } else {
            return new SimpleDateFormat(format, Locale.ENGLISH).format(new Date());
        }
        //return new SimpleDateFormat(format).format(new Date());
    }

    public static String getEditTextValue(EditText editText) {

        return editText.getText().toString();

    }

    public static String getTextViewValue(TextView textView) {

        return textView.getText().toString().trim();

    }

    public static String getOsVersion() {

        return Build.VERSION.RELEASE + "";
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String firstCapital(String input) {

        return Character.toUpperCase(input.charAt(0)) + input.substring(1);

    }

    public static String firstCapitalName(String input) {

        try {
            return Character.toUpperCase(input.charAt(0)) + (input.substring(1)).toLowerCase();
        } catch (Exception e) {
            // TODO: handle exception
            return "";
        }


    }

    public static final LinkedHashMap<String, Integer> countyMap = new LinkedHashMap<String, Integer>();

    static {
        countyMap.put("Appling", 6);
        countyMap.put("Atkinson", 7);
        countyMap.put("Bacon", 8);
        countyMap.put("Baker", 9);
        countyMap.put("Baldwin", 10);
        countyMap.put("Banks", 395);
        countyMap.put("Barrow", 396);
        countyMap.put("Bartow", 397);
        countyMap.put("Ben Hill", 398);
        countyMap.put("Berrien", 399);
        countyMap.put("Bibb", 400);
        countyMap.put("Bleckley", 401);
        countyMap.put("Brantley", 402);
        countyMap.put("Brooks", 403);
        countyMap.put("Bryan", 404);
        countyMap.put("Bulloch", 405);
        countyMap.put("Burke", 406);
        countyMap.put("Butts", 407);
        countyMap.put("Calhoun", 408);
        countyMap.put("Camden", 409);
        countyMap.put("Candler", 410);
        countyMap.put("Carroll", 411);
        countyMap.put("Catoosa", 412);
        countyMap.put("Charlton", 413);
        countyMap.put("Chatham", 414);
        countyMap.put("Chattahoochee", 415);
        countyMap.put("Chattooga", 416);
        countyMap.put("Cherokee", 417);
        countyMap.put("Clarke", 418);
        countyMap.put("Clay", 419);
        countyMap.put("Clayton", 420);
        countyMap.put("Clinch", 421);
        countyMap.put("Cobb", 422);
        countyMap.put("Coffee", 423);
        countyMap.put("Colquitt", 424);
        countyMap.put("Columbia", 425);
        countyMap.put("Cook", 426);
        countyMap.put("Coweta", 427);
        countyMap.put("Crawford", 428);
        countyMap.put("Crisp", 429);
        countyMap.put("Dade", 430);
        countyMap.put("Dawson", 431);
        countyMap.put("Decatur", 432);
        countyMap.put("DeKalb", 433);
        countyMap.put("Dodge", 434);
        countyMap.put("Dooly", 435);
        countyMap.put("Dougherty", 436);
        countyMap.put("Douglas", 437);
        countyMap.put("Early", 438);
        countyMap.put("Echols", 439);
        countyMap.put("Effingham", 440);
        countyMap.put("Elbert", 441);
        countyMap.put("Emanuel", 442);
        countyMap.put("Evans", 443);
        countyMap.put("Fannin", 444);
        countyMap.put("Fayette", 445);
        countyMap.put("Floyd", 446);
        countyMap.put("Forsyth", 447);
        countyMap.put("Franklin", 448);
        countyMap.put("Fulton", 449);
        countyMap.put("Gilmer", 450);
        countyMap.put("Glascock", 451);
        countyMap.put("Glynn", 452);
        countyMap.put("Gordon", 453);
        countyMap.put("Grady", 454);
        countyMap.put("Greene", 455);
        countyMap.put("Gwinnett", 456);
        countyMap.put("Habersham", 457);
        countyMap.put("Hall", 458);
        countyMap.put("Hancock", 459);
        countyMap.put("Haralson", 460);
        countyMap.put("Harris", 461);
        countyMap.put("Hart", 462);
        countyMap.put("Heard", 463);
        countyMap.put("Henry", 464);
        countyMap.put("Houston", 465);
        countyMap.put("Irwin", 466);
        countyMap.put("Jackson", 467);
        countyMap.put("Jasper", 468);
        countyMap.put("Jeff Davis", 469);
        countyMap.put("Jefferson", 470);
        countyMap.put("Jenkins", 471);
        countyMap.put("Johnson", 472);
        countyMap.put("Jones", 473);
        countyMap.put("Lamar", 474);
        countyMap.put("Lanier", 475);
        countyMap.put("Laurens", 476);
        countyMap.put("Lee", 477);
        countyMap.put("Liberty", 478);
        countyMap.put("Lincoln", 479);
        countyMap.put("Long", 480);
        countyMap.put("Lowndes", 481);
        countyMap.put("Lumpkin", 482);
        countyMap.put("Macon", 483);
        countyMap.put("Madison", 484);
        countyMap.put("Marion", 485);
        countyMap.put("McDuffie", 486);
        countyMap.put("McIntosh", 487);
        countyMap.put("Meriwether", 488);
        countyMap.put("Miller", 489);
        countyMap.put("Mitchell", 490);
        countyMap.put("Monroe", 491);
        countyMap.put("Montgomery", 492);
        countyMap.put("Morgan", 493);
        countyMap.put("Murray", 494);
        countyMap.put("Muscogee", 495);
        countyMap.put("Newton", 496);
        countyMap.put("Oconee", 497);
        countyMap.put("Oglethorpe", 498);
        countyMap.put("Paulding", 499);
        countyMap.put("Peach", 500);
        countyMap.put("Pickens", 501);
        countyMap.put("Pierce", 502);
        countyMap.put("Pike", 503);
        countyMap.put("Polk", 504);
        countyMap.put("Pulaski", 505);
        countyMap.put("Putnam", 506);
        countyMap.put("Quitman", 507);
        countyMap.put("Rabun", 508);
        countyMap.put("Randolph", 509);
        countyMap.put("Richmond", 510);
        countyMap.put("Rockdale", 511);
        countyMap.put("Schley", 512);
        countyMap.put("Screven", 513);
        countyMap.put("Seminole", 514);
        countyMap.put("Spalding", 515);
        countyMap.put("Stephens", 516);
        countyMap.put("Stewart", 517);
        countyMap.put("Sumter", 518);
        countyMap.put("Talbot", 519);
        countyMap.put("Taliaferro", 520);
        countyMap.put("Tattnall", 521);
        countyMap.put("Taylor", 522);
        countyMap.put("Telfair", 523);
        countyMap.put("Terrell", 524);
        countyMap.put("Thomas", 525);
        countyMap.put("Tift", 526);
        countyMap.put("Toombs", 527);
        countyMap.put("Towns", 528);
        countyMap.put("Treutlen", 529);
        countyMap.put("Troup", 530);
        countyMap.put("Turner", 531);
        countyMap.put("Twiggs", 532);
        countyMap.put("Union", 533);
        countyMap.put("Upson", 534);
        countyMap.put("Walker", 535);
        countyMap.put("Walton", 536);
        countyMap.put("Ware", 537);
        countyMap.put("Warren", 538);
        countyMap.put("Washington", 539);
        countyMap.put("Wayne", 540);
        countyMap.put("Webster", 541);
        countyMap.put("Wheeler", 542);
        countyMap.put("White", 543);
        countyMap.put("Whitfield", 544);
        countyMap.put("Wilcox", 545);
        countyMap.put("Wilkes", 546);
        countyMap.put("Wilkinson", 547);
        countyMap.put("Worth", 548);
        countyMap.put("OTHER", 9999);
    }

    public static final LinkedHashMap<String, Integer> questionMap = new LinkedHashMap<String, Integer>();

    static {
        questionMap.put("What is your mother's maiden name?", 1);
        questionMap.put("What was the name of your first (elementary) school?", 2);
        questionMap.put("What is your brother's middle name?", 3);
        questionMap.put("What is your sister's middle name?", 4);
        questionMap.put("In what city were you born?", 5);
        questionMap.put("What was the make of your first car?", 6);
        questionMap.put("What is the name of your first pet?", 7);
        questionMap.put("What street did you grow up on?", 8);
    }

    public static CharSequence[] returnYears() {
        CharSequence[] array_3 = {
                "2015", "2016", "2017", "2018", "2019", "2020", "2021",
                "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029",
                "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037",
                "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045",
                "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053",
                "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061",
                "2062", "2063", "2064", "2065"};

        return array_3;
    }

    public static void openLinkInBrowser(String URL, Activity activity) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
        activity.startActivity(browserIntent);
    }

    /* Below Method is used to Convert time format from HH:MM to hh:mm a ("input, HH:MM, hh:mm a")*/
    public static String getNewTimeFormat(String inputTime, String inputTimeFormat, String outputTimeFormat) {

        try {
            String originalString = inputTime;
            Date date = new SimpleDateFormat(inputTimeFormat).parse(originalString);
            String newString = new SimpleDateFormat(outputTimeFormat).format(date); // 9:00
            return newString;
        } catch (Exception e) {
            return "";
        }

    }

    /* Below Method is used to compare Time ("time1, time2")*/
    public static boolean compareTime(String time1, String time2) throws Exception {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        SimpleDateFormat sdf;
        EpaLog.e("Utils", "currentapiVersion--* " + currentapiVersion);
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if ((time1.contains("AM") || time1.contains("PM")) || (time2.contains("AM") || time2.contains("PM"))) {
                // do something for phones running an SDK before Marshmallow
                sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            } else {
                // Do something for Marshmallow and above versions
                sdf = new SimpleDateFormat("hh:mm a", Locale.UK);
            }
        } else {
            // do something for phones running an SDK before Marshmallow
            sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        }

        //SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

        Date date1 = sdf.parse(time1);
        Date date2 = sdf.parse(time2);

        Log.e("date1", "----->:" + date1);
        Log.e("date2", "----->:" + date2);

        if (date1.compareTo(date2) > 0) {
            return false;
        } else if (date1.compareTo(date2) < 0) {
            return true;
        } else if (date1.compareTo(date2) == 0) {
            return false;
        } else {
            return false;
        }
    }

    /* Below Method is used to compare Time ("time1, time2")*/
    public static boolean compareTime24(String time1, String time2) throws Exception {
        //    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date date1 = sdf.parse(time1);
        Date date2 = sdf.parse(time2);

        Log.e("date1", "----->:" + date1);
        Log.e("date2", "----->:" + date2);

        if (date1.compareTo(date2) > 0) {
            return false;
        } else if (date1.compareTo(date2) < 0) {
            return true;
        } else if (date1.compareTo(date2) == 0) {
            return false;
        } else {
            return false;
        }
    }

    /* Below Method is used to generate Oauth Nonce ("int")*/
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static Random rnd = new Random();

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    void doStuff() {
        //generate oauth_signature, nonce timestamp

    }

    /* Below Methods is used to generate Oauth Signature via (Signature method, Consumer key) */
  /*  public static String hmacSha1() {

        try {
            String type = "HmacSHA1";
            SecretKeySpec secret = new SecretKeySpec(WebUtils.CONSUMER_SECRET.getBytes(), type);
            Mac mac = Mac.getInstance(type);
            mac.init(secret);
            String base_string = "POST&https%3A%2F%2Flocalhost%2Frary%2Fapi%2Fgetprofile&oauth_consumer_key%3DD66YA9a963btXuyzh2ucuh3SUU4f7397%26oauth_nonce%3DVHpw2t%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1457719817%26oauth_token%3D" + Utils.TOKEN_TEMP + "%26oauth_version%3D1.0";
            byte[] bytes = mac.doFinal(base_string.getBytes());

            return bytesToHex(bytes);
        } catch (Exception e) {
            return "";
        }

    }*/

    private final static char[] hexArray = "0123456789abcdef".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static void initializeFontsNormalEditText(final Context context, EditText editText, String fontFile) {
        Typeface FONT_NORMAL = Typeface.createFromAsset(context.getAssets(), fontFile);
        editText.setTypeface(FONT_NORMAL);
    }

    public static void initializeFontsNormalTextView(final Context context, TextView textView, String fontFile) {
        Typeface FONT_NORMAL = Typeface.createFromAsset(context.getAssets(), fontFile);
        textView.setTypeface(FONT_NORMAL);
    }

    public static void initializeFontsNormalButton(final Context context, Button button, String fontFile) {
        Typeface FONT_NORMAL = Typeface.createFromAsset(context.getAssets(), fontFile);
        button.setTypeface(FONT_NORMAL);
    }

    /* Below Method is used to get requestBody To String *//*
    private static String requestBodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }*/

    /* Below Methods is used to get Current System time in millis */
    public static long getSystemTime() {
        long l = System.currentTimeMillis() / 1000;
        return l;
    }

    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%d:%02d:%02d", h, m, s);

    }

    public static String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return "";
    }

    public static String getString(EditText editText) {

        return editText.getText().toString().trim();
    }

    public static InputStream getInputstream(String data) {
        InputStream is = new ByteArrayInputStream(data.getBytes());

        return is;
    }

    public static ProgressDialog showProgressDialig(String title, String message, boolean flag, Context context) {

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
        progressDialog.setCancelable(flag);

        return progressDialog;
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


    public static boolean isPasswordMatching(String password, String confirmPassword) {
        Pattern pattern = Pattern.compile(password, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(confirmPassword);

        if (!matcher.matches()) {
            // do your Toast("passwords are not matching");

            return false;
        }

        return true;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static String convertDateToTimestamp(String someDate, String format) {
        try {
            /*String someDate = dateStr;*/
            SimpleDateFormat sdf;
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            EpaLog.e("Utils", "currentapiVersion--* " + currentapiVersion);
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {

                if (someDate.contains("AM") || someDate.contains("PM")) {
                    // do something for phones running an SDK before Marshmallow
                    sdf = new SimpleDateFormat(format, Locale.ENGLISH);
                    EpaLog.e("convertDateToTimestamp", "0");
                } else {
                    // Do something for Marshmallow and above versions
                    sdf = new SimpleDateFormat(format, Locale.UK);
                    EpaLog.e("convertDateToTimestamp", "1");
                }
            } else {
                // do something for phones running an SDK before Marshmallow
                sdf = new SimpleDateFormat(format, Locale.ENGLISH);
                EpaLog.e("convertDateToTimestamp", "2");
            }
            //sdf = new SimpleDateFormat(format, Locale.ENGLISH);
            EpaLog.e("convertDateToTimestamp", "someDate--* " + someDate);
            Date date = sdf.parse(someDate);
            EpaLog.e("Utils", "date.getTime()-- " + date.getTime());
            return date.getTime() / 1000 + "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getDateFromTimeStamp(long time, String format) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = android.text.format.DateFormat.format(format, cal).toString();
        return date;
    }

    public static Date convertStringToDate(String dateStr, String format) {
        try {

            String someDate = dateStr;
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date = sdf.parse(someDate);
            System.out.println(date.getTime());
            return date;
        } catch (Exception e) {
            return new Date();
        }
    }


    public static Bitmap getImageFromBase64(String decode) {
        byte[] decodedString = Base64.decode(decode, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static int getIntegerFromString(String parse) {
        try {
            Integer integer = Integer.parseInt(parse);
            return integer;
        } catch (Exception e) {
            return 0;
        }

    }

    public static double getDoubleFromString(String value) {
        double parseDouble;
        try {
            parseDouble = Double.parseDouble(value); // Make use of autoboxing.  It's also easier to read.
   //         EpaLog.e("Utils", "parseDouble" + parseDouble);
            return parseDouble;
        } catch (NumberFormatException e) {
            // p did not contain a valid double
        }
        return 0;
    }

    public static String getStringFromDouble(double value) {
        String parseString;
        try {
            parseString = String.valueOf(value); // Make use of autoboxing.  It's also easier to read.
          //  EpaLog.e("Utils", "parseDouble" + parseString);
            return parseString;
        } catch (Exception e) {
            // p did not contain a valid double
        }
        return "";
    }

    public static long getLongFromString(String parse) {
        try {
            Long integer = Long.parseLong(parse);
            return integer;
        } catch (Exception e) {
            return 0;
        }

    }

   /* public static String getUserToken(Context context) {

        return SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.ACCESS_TOKEN);
    }

    public static String getSharedPrefValue(Context context, String key) {

        return SharedPreferenceHelper.getPreference(context, key);
    }

    public static void saveSharedPrefValue(Context context, String key, String value) {

        SharedPreferenceHelper.savePreferences(key, value, context);
    }*/

    public static String getBase64FromImage(ImageView imv) {

        imv.buildDrawingCache();
        Bitmap bitmap = imv.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
        byte[] image = stream.toByteArray();
        //System.out.println("byte array:"+image);

        String img_str = Base64.encodeToString(image, 0);
        //System.out.println("string:"+img_str);

        return img_str;

    }

/*    public static String hmacSha2() {

        try {
            String type = "HmacSHA1";
            SecretKeySpec secret = new SecretKeySpec((WebUtils.CONSUMER_SECRET + "&" + TOKEN_TEMP_SECRET).getBytes(), type);
            Mac mac = Mac.getInstance(type);
            mac.init(secret);
            String base_string = "POST&http%3A%2F%2Fdevelopment.azularc.com%2Frary%2Fapi%2Fgetprofile&oauth_consumer_key%3DD66YA9a963btXuyzh2ucuh3SUU4f7397%26oauth_nonce%3DVHpw2t%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1457719817%26oauth_token%3D" + Utils.TOKEN_TEMP + "%26oauth_version%3D1.0";
            Log.e("rary", "base_string--" + base_string);
            byte[] bytes = mac.doFinal(base_string.getBytes());
            return encodeBase64(bytesToHex(bytes));
        } catch (Exception e) {
            return "";
        }

    }*/

    public static String encodeBase64(String string) {
        try {
            byte[] data = string.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e) {
            return "";
        }

    }

    public static double checkTimeDifference(String time1, String time2, String pattern) {

        try {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            SimpleDateFormat format;
            EpaLog.e("Utils", "currentapiVersion--* " + currentapiVersion);
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                if ((time1.contains("AM") || time1.contains("PM")) || (time2.contains("AM") || time2.contains("PM"))) {
                    // do something for phones running an SDK before Marshmallow
                    format = new SimpleDateFormat(pattern, Locale.ENGLISH);
                } else {
                    // Do something for Marshmallow and above versions
                    format = new SimpleDateFormat(pattern, Locale.UK);
                }
            } else {
                // do something for phones running an SDK before Marshmallow
                format = new SimpleDateFormat(pattern, Locale.ENGLISH);
            }
            //SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
            Date Date1 = format.parse(time1);
            Date Date2 = format.parse(time2);
            double mills = Date2.getTime() - Date1.getTime();
            EpaLog.e("Utils", "Date1 " + Date1);
            EpaLog.e("Utils", "Date2 " + Date2);
            EpaLog.e("Utils", "mills " + mills);
            EpaLog.e("Utils", "mills / (1000 * 60 * 60--> " + mills / (1000 * 60 * 60));
            return (mills / (1000 * 60 * 60));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;

    }

    /**
     * Turns a period of time into the number of minutes represented
     * (eg. 06:30:15 returns 360.25)
     *
     * @param hourFormat The string containing the hour format "HH:MM:SS"
     * @return The number of minutes represented, or -1 if the date could not be processed.
     */
    public static double parseTimeToMinutes(String hourFormat) {

        double minutes = 0;
        String[] split = hourFormat.split(":");

        try {

            // minutes += Double.parseDouble(split[0])*60;
            minutes += Double.parseDouble(split[1]);
            minutes += Double.parseDouble(split[2]) / 60;
            return minutes;

        } catch (Exception e) {
            return -1;
        }

    }

    public static long checkTimeDifferenceInFormat(String time1, String time2, String pattern) {

        try {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            SimpleDateFormat format;
            EpaLog.e("Utils", "currentapiVersion--* " + currentapiVersion);
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                if ((time1.contains("AM") || time1.contains("PM")) || (time2.contains("AM") || time2.contains("PM"))) {
                    // do something for phones running an SDK before Marshmallow
                    format = new SimpleDateFormat(pattern, Locale.ENGLISH);
                } else {
                    // Do something for Marshmallow and above versions
                    format = new SimpleDateFormat(pattern, Locale.UK);
                }
            } else {
                // do something for phones running an SDK before Marshmallow
                format = new SimpleDateFormat(pattern, Locale.ENGLISH);
            }
            //SimpleDateFormat format = new SimpleDateFormat(pattern);
            Date Date1 = format.parse(time1);
            Date Date2 = format.parse(time2);
            long mills = Date2.getTime() - Date1.getTime();
            return mills / 1000;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;

    }

    public static int getEdtValLength(EditText editText) {

        return editText.getText().toString().trim().length();
    }

    public static String getEdtVal(EditText editText) {

        return editText.getText().toString().trim();
    }

    public static int getTxtValLength(TextView textView) {

        return textView.getText().toString().trim().length();
    }

    public static String getTxtVal(TextView textView) {

        return textView.getText().toString().trim();
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    public static boolean checkEmailCorrect(String email) {
        if (email.length() == 0) {
            return false;
        }
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static String compressImage(String imageUri, Context context) {

        String filePath = getRealPathFromURI(imageUri, context);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        EpaLog.e("Utils", "actualHeight" + actualHeight);
        EpaLog.e("Utils", "actualWidth" + actualWidth);
//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);

        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
/*
//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            EpaLog.e("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                EpaLog.e("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                EpaLog.e("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                EpaLog.e("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);

        } catch (IOException e) {
            e.printStackTrace();
        }*/

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 30, out);
            EpaLog.e("Utils", "getByteCount--*3" + scaledBitmap.getByteCount());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;
    }

    private static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Rary/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private static String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static String getBase64FromFile(String filePath) {

        try {
            if (filePath == null)
                return "";
            Bitmap bm = BitmapFactory.decodeFile(filePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 30, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();

            return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public static String removeLastCharacted(String str, char character) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == character) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }


    public static String getDateFromTimestamp(String time, String format) {
//        try {
//
//            Calendar cal = Calendar.getInstance();
//            TimeZone tz = cal.getTimeZone();//get your local time zone.
//            SimpleDateFormat sdf = new SimpleDateFormat(format);
//            sdf.setTimeZone(tz);//set time zone.
//            String localTime = sdf.format(new Date(Long.parseLong(time)));
//            Date date = new Date();
//            try {
//                date = sdf.parse(localTime);//get local date
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            return date.toString();
//        } catch (Exception e) {
//            return "";
//        }

        /*try {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            Calendar cal;
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                cal = Calendar.getInstance(Locale.UK);
            } else {
                cal = Calendar.getInstance(Locale.ENGLISH);
            }
            //Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.parseLong(time.replaceAll(".000000", "")) * 1000);
            String date = android.text.format.DateFormat.format(format, cal).toString();
            return date;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }*/

        try {
            SimpleDateFormat dateFormat;
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            Calendar cal1 = Calendar.getInstance();
            cal1.setTimeInMillis(Long.parseLong(time.replaceAll(".000000", "")) * 1000);
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                dateFormat = new SimpleDateFormat(format, Locale.UK);
            } else {
                dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
            }
            //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            String dateforrow = dateFormat.format(cal1.getTime());
            return dateforrow;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }

    public static String getTimeFromTimestamp(long time) {

        try {
            long timeInMillis = time;
            SimpleDateFormat dateFormat;
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            Calendar cal1 = Calendar.getInstance();
            cal1.setTimeInMillis(timeInMillis * 1000);
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                dateFormat = new SimpleDateFormat("HH:mm", Locale.UK);
            } else {
                dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            }
            //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            String dateforrow = dateFormat.format(cal1.getTime());
            return dateforrow;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTimeHHMMFromTimestamp(long time) {

        try {
            long timeInMillis = time;
            SimpleDateFormat dateFormat;
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            Calendar cal1 = Calendar.getInstance();
            cal1.setTimeInMillis(timeInMillis * 1000);
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                dateFormat = new SimpleDateFormat("hh:mm a", Locale.UK);
            } else {
                dateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            }
            //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            String dateforrow = dateFormat.format(cal1.getTime());
            return dateforrow;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateFromTimestamp(String time) {
        try {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            Calendar cal;
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                cal = Calendar.getInstance(Locale.UK);
            } else {
                cal = Calendar.getInstance(Locale.ENGLISH);
            }
            // Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            TimeZone tz = cal.getTimeZone();
            cal.setTimeZone(tz);
            cal.setTimeInMillis(Long.parseLong(time) * 1000);
            String date = android.text.format.DateFormat.format("hh:mm a", cal).toString();
            return date;
        } catch (Exception e) {
            return "";
        }
    }

    public static long getDifferenceInDays(String inputString1, String inputString2, String format) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        SimpleDateFormat myFormat;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            myFormat = new SimpleDateFormat(format, Locale.UK);
        } else {
            myFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        }
        //SimpleDateFormat myFormat = new SimpleDateFormat(format);

        try {
            Date date1 = myFormat.parse((Long.parseLong(inputString1) * 1000) + "");
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();

            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String todaysDate() {
        Calendar calender = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMM");
        String strDate = sdf.format(calender.getTime());
        String strDateFinal = strDate.replaceAll(":", "\n");
        StringBuilder sb = new StringBuilder(strDateFinal);
        for (int index = 0; index < sb.length(); index++) {
            char c = sb.charAt(index);
            if (Character.isLowerCase(c)) {
                sb.setCharAt(index, Character.toUpperCase(c));
            } /*else {
                sb.setCharAt(index, Character.toLowerCase(c));
            }*/
        }
        return sb.toString();
    }

    public static String convertLowerToUpper(String input) {
        String strDateFinal = input.replaceAll(" ", "\n");
        StringBuilder sb = new StringBuilder(strDateFinal);
        for (int index = 0; index < sb.length(); index++) {
            char c = sb.charAt(index);
            if (Character.isLowerCase(c)) {
                sb.setCharAt(index, Character.toUpperCase(c));
            } /*else {
                sb.setCharAt(index, Character.toLowerCase(c));
            }*/
        }
        return sb.toString();
    }

    public static String convertUpperToLower(String input) {
        String strDateFinal = input.replaceAll(" ", "");
        StringBuilder sb = new StringBuilder(strDateFinal);
        for (int index = 0; index < sb.length(); index++) {
            char c = sb.charAt(index);
            if (Character.isLowerCase(c)) {
                sb.setCharAt(index, Character.toLowerCase(c));
            } /*else {
                sb.setCharAt(index, Character.toLowerCase(c));
            }*/
        }
        return sb.toString();
    }

    public static long getTimeZoneOffset() {
        TimeZone timezone = TimeZone.getTimeZone(TimeZone.getDefault().getID());

        // checking offset value for date
        return timezone.getOffset(Calendar.ZONE_OFFSET);
    }

    public static String getCurrentSeconds() {

        return (long) System.currentTimeMillis() / 1000 + "";
    }

    public static int compareDates(String time1, String time2, String format) throws Exception {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        SimpleDateFormat sdf;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            sdf = new SimpleDateFormat(format, Locale.UK);
        } else {
            sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        }
        //SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);

        Date date1 = sdf.parse(time1);
        Date date2 = sdf.parse(time2);

        Log.e("date1", "----->:" + date1);
        Log.e("date2", "----->:" + date2);

        //date1 is greater than date2
        if (date1.compareTo(date2) > 0) {
            return 0;
        }
        //date1 is less than date2
        else if (date1.compareTo(date2) < 0) {
            return 1;
            //date1 is equal to date2
        } else if (date1.compareTo(date2) == 0) {

            return 2;

        } else {
            return 0;
        }
    }

    /* Below Method is used to compare Time ("time1, time2")*/
    public static boolean compareTimeForValidation(String breakTime, String actualTime) throws Exception {

        String breakTimeArray[] = breakTime.split("\\:");
        String actualTimeArray[] = actualTime.split("\\:");

        EpaLog.e("rary", "breakTimeArray--" + breakTimeArray);
        EpaLog.e("rary", "actualTimeArray--" + actualTimeArray);

        int brkHr = Integer.parseInt(breakTimeArray[0]);
        int brkMin = Integer.parseInt(breakTimeArray[1]);

        int actHr = Integer.parseInt(actualTimeArray[0]);
        int actMin = Integer.parseInt(actualTimeArray[1]);

        int totalBreakMins = (brkHr * 60) + brkMin;

        int totalActualMins = (actHr * 60) + actMin;

        EpaLog.e("rary", "totalBreakMins--" + totalBreakMins);
        EpaLog.e("rary", "totalActualMins--" + totalActualMins);


        if (totalBreakMins <= totalActualMins)
            return true;

        return false;


//        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",Locale.ENGLISH);
//
//
//        Date date1 = sdf.parse(breakTime);
//        Date date2 = sdf.parse(actualTime);
//
//        Log.e("date1", "----->:" + date1);
//        Log.e("date2", "----->:" + date2);
//
//        if (date1.compareTo(date2) > 0) {
//            return false;
//        } else if (date1.compareTo(date2) < 0) {
//            return true;
//        } else if (date1.compareTo(date2) == 0) {
//
//
//            return false;
//
//        } else {
//            return false;
//        }
    }

    public static String getFirstDay(String jobDates) {

        String dateSplit[] = jobDates.split(",");
        for (String obj : dateSplit) {

            return obj;
        }
        return "";
    }

    public static double meterDistanceBetweenPoints(float latA, float lngA, float latB, float lngB) {

        Location locationA = new Location("point A");

        locationA.setLatitude(latA);
        locationA.setLongitude(lngA);
        EpaLog.e("Utils", "distance" + locationA);

        Location locationB = new Location("point B");
        locationB.setLatitude(latB);
        locationB.setLongitude(lngB);
        EpaLog.e("Utils", "distance" + locationB);

        float distance = locationA.distanceTo(locationB);
        EpaLog.e("Utils", "distance" + distance);

        return Double.parseDouble(String.format("%.2f", (distance * 0.000621371)));


    }

    public static boolean compareDate(Date obj1, Date obj2, int type) {

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        calendar1.setTime(obj1);
        calendar2.setTime(obj2);

        int compare = calendar1.compareTo(calendar2);

        if (compare == 0 && type == 0)
            return true;

        if (compare == 0 && type == 1)
            return false;

        if (compare < 0)
            return false;

        if (compare > 0)
            return true;

        return false;
    }


    public static String getCurrentTimezoneId() {
        String timeZoneName = TimeZone.getDefault().getID();

        return timeZoneName;
    }

    public static int convertDateToOriginalTimestamp() {
        try {
            Date date = new Date();
            int offset = date.getTimezoneOffset() * 60;
            return offset;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     * @param status  - success/failure (used to set icon)
     */
    public static void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static String addTimeToDateObject(String orgTime, int increment, int type) {
        try {
            String dt = orgTime;  // Start date
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            SimpleDateFormat sdf;
            if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                if (orgTime.contains("AM") || orgTime.contains("PM")) {
                    sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
                } else {
                    sdf = new SimpleDateFormat("hh:mm a", Locale.UK);
                }
            } else {
                sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            }
            //SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dt));
            if (type == Calendar.HOUR)
                c.add(Calendar.HOUR, increment);  // number of hours to add
            if (type == Calendar.MINUTE)
                c.add(Calendar.MINUTE, increment);
            dt = sdf.format(c.getTime());  // dt is now the new date
            return dt;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static boolean checkGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                //gApi.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //Toast.makeText(activity, "This device is not supported.", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

    public static boolean getEditTextValidation(EditText editText) {

        String val = editText.getText().toString().trim();

        if (val.length() > 0)
            return true;
        return false;
    }

    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String setDayFormat(String day) {
        if (day.contains("Mon")) {
            return "Monday";
        } else if (day.contains("Tue")) {
            return "Tuesday";
        } else if (day.contains("Wed")) {
            return "Wednesday";
        } else if (day.contains("Thu")) {
            return "Thursday";
        } else if (day.contains("Fri")) {
            return "Friday";
        } else if (day.contains("Sat")) {
            return "Saturday";
        } else if (day.contains("Sun")) {
            return "Sunday";
        }
        return "";
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static float determineScreenDensity(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float density = metrics.density;

//        if (density==DisplayMetrics.DENSITY_HIGH) {
//        //    Toast.makeText(activity, "DENSITY_HIGH: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
//        }
//        else if (density==DisplayMetrics.DENSITY_MEDIUM) {
//        //    Toast.makeText(activity, "DENSITY_MEDIUM: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
//        }
//        else if (density==DisplayMetrics.DENSITY_LOW) {
//        //    Toast.makeText(activity, "DENSITY_LOW:" + String.valueOf(density),  Toast.LENGTH_LONG).show();
//        }
//        else {
//         //   Toast.makeText(activity, "Density is neither HIGH, MEDIUM OR LOW: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
//        }
        return density;
    }

}
