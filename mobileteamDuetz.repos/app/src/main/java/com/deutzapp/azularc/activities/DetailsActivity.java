package com.deutzapp.azularc.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deutzapp.R;
import com.deutzapp.azularc.data.ServiceProviderDetailsRequest;
import com.deutzapp.azularc.data.Serviceproviderdetail;
import com.deutzapp.azularc.utils.ConnectionDetector;
import com.deutzapp.azularc.utils.EpaLog;
import com.deutzapp.azularc.utils.Utils;
import com.deutzapp.azularc.utils.WebUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class DetailsActivity extends AppCompatActivity {

    Context context;
    TextView bt_back, bt_backs, bt_next, tv_name, tv_type, tv_website_link, tv_address_icon, tv_address, tv_address_details, tv_phone_icon, tv_phone, tv_phone_details, tv_24_phone_icon, tv_24_phone, tv_24_phone_details, tv_email_icon, tv_email, tv_email_details, tv_oem_dealer_icon, tv_oem_dealer, tv_oem_dealer_details;
    LinearLayout ll_wallpaper, ll_24_contact, ll_oem_dealer;
    //View vv_24_contact, vv_address;
    ImageView ic_header;
    Typeface face;
    LatLng locationData;
    public static final String TAG = "DetailsActivity";
    Boolean isInternetPresent = false;
    ConnectionDetector connectionDetector;
    String providerId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);
        context = this;

        connectionDetector = new ConnectionDetector(context);
        bt_backs = (TextView) findViewById(R.id.bt_backs);
        bt_back = (TextView) findViewById(R.id.bt_back);
        bt_next = (TextView) findViewById(R.id.bt_next);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_type = (TextView) findViewById(R.id.tv_type);
        tv_website_link = (TextView) findViewById(R.id.tv_website_link);
        tv_address_icon = (TextView) findViewById(R.id.tv_address_icon);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_address_details = (TextView) findViewById(R.id.tv_address_details);
        tv_phone_icon = (TextView) findViewById(R.id.tv_phone_icon);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_phone_details = (TextView) findViewById(R.id.tv_phone_details);
        tv_24_phone_icon = (TextView) findViewById(R.id.tv_24_phone_icon);
        tv_24_phone = (TextView) findViewById(R.id.tv_24_phone);
        tv_24_phone_details = (TextView) findViewById(R.id.tv_24_phone_details);
        tv_email_icon = (TextView) findViewById(R.id.tv_email_icon);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_email_details = (TextView) findViewById(R.id.tv_email_details);
        tv_oem_dealer_icon = (TextView) findViewById(R.id.tv_oem_dealer_icon);
        tv_oem_dealer = (TextView) findViewById(R.id.tv_oem_dealer);
        tv_oem_dealer_details = (TextView) findViewById(R.id.tv_oem_dealer_details);

        ic_header = (ImageView) findViewById(R.id.ic_header);

        ll_wallpaper = (LinearLayout) findViewById(R.id.ll_wallpaper);
        ll_24_contact = (LinearLayout) findViewById(R.id.ll_24_contact);
        ll_oem_dealer = (LinearLayout) findViewById(R.id.ll_oem_dealer);
//
//        vv_24_contact = (View) findViewById(R.id.vv_24_contact);
//        vv_address = (View) findViewById(R.id.vv_address);

        face = Typeface.createFromAsset(context.getAssets(),
                "fontawesome-webfont.ttf");

        bt_back.setTypeface(face);
        bt_back.setText(R.string.left_angle);

        bt_next.setTypeface(face);
        bt_next.setText(R.string.external_link);

        tv_address_icon.setTypeface(face);
        tv_address_icon.setText(R.string.map_icon);

        tv_phone_icon.setTypeface(face);
        tv_phone_icon.setText(R.string.phone_icon);

        tv_24_phone_icon.setTypeface(face);
        tv_24_phone_icon.setText(R.string.time_icon);

        tv_email_icon.setTypeface(face);
        tv_email_icon.setText(R.string.email_icon);

        tv_oem_dealer_icon.setTypeface(face);
        tv_oem_dealer_icon.setText(R.string.oem_icon);

        try {
            providerId = getIntent().getStringExtra(Utils.PROVIDERID);
            EpaLog.e(TAG, "providerId-- " + providerId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_website_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = Utils.getTextViewValue(tv_website_link);
                if (url.length() > 1) {
                    if (url.contains("http://") || url.contains("https://")) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    } else {
                        StringBuilder builder = new StringBuilder();
                        builder.append("http://");
                        builder.append(url);
                        EpaLog.e(TAG, "builder-- " + builder);
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(builder.toString()));
                        startActivity(i);
                    }
                }

            }
        });

        tv_address_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (locationData != null) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + locationData.latitude + "," + locationData.longitude));
                    //  Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + "Kailash Lounge, Park Site Road, Mumbai, Maharashtra"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Utils.showToast(context, "Unable to fetch job location.");
                }
            }
        });

        tv_phone_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.getTextViewValue(tv_phone_details).length() > 1) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Do you want to call" + " " + Utils.getTextViewValue(tv_phone_details) + "?");

                    alertDialogBuilder.setPositiveButton("Call", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (Build.VERSION.SDK_INT >= 23 &&
                                    ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        1);

                            } else {
                                Utils.callThisNumber(Utils.getTextViewValue(tv_phone_details), context);
                            }
                        }
                    });

                    alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }

            }
        });

        tv_24_phone_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.getTextViewValue(tv_24_phone_details).length() > 1) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Do you want to call" + " " + Utils.getTextViewValue(tv_24_phone_details) + "?");
                    alertDialogBuilder.setPositiveButton("Call", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (Build.VERSION.SDK_INT >= 23 &&
                                    ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        1);

                            } else {
                                Utils.callThisNumber(Utils.getTextViewValue(tv_24_phone_details), context);
                            }
                        }
                    });

                    alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            }
        });

        tv_email_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.getTextViewValue(tv_email_details).length() > 1) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", Utils.getTextViewValue(tv_email_details), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                }

            }
        });

        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String webSite = Utils.getTextViewValue(tv_website_link);
                String name = Utils.getTextViewValue(tv_name);
                String type = Utils.getTextViewValue(tv_type);
                String address = Utils.getTextViewValue(tv_address_details);
                String phone = Utils.getTextViewValue(tv_phone_details);
                String phone24 = Utils.getTextViewValue(tv_24_phone_details);
                String email_details = Utils.getTextViewValue(tv_email_details);
                // String oem_details = Utils.getTextViewValue(tv_oem_dealer_details);
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                //  emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Details");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Name : " + name + "\nType : " + type + "\nWebsite : " + webSite + "\nAddress : " + address + "\nPhone : " + phone + "\n24 hour Phone : " + phone24 + "\nEmail : " + email_details);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });

        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bt_backs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        isInternetPresent = connectionDetector.isConnectingToInternet();
        EpaLog.e(TAG, "isInternetPresent- " + isInternetPresent);
        if (isInternetPresent) {
            getServiceProviderDetails();

        } else {
            Utils.showAlertDialog(context, "No Internet Connection",
                    "You don't have internet connection.", false);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getServiceProviderDetails() {

        new AsyncTask<ServiceProviderDetailsRequest, Void, Response>() {

            ProgressDialog progressDialog;
            Gson gson = new Gson();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = Utils.showProgressDialig(Utils.PLEASE_WAIT, Utils.FETCHING_DETAILS, false, context);

            }

            @Override
            protected Response doInBackground(ServiceProviderDetailsRequest... params) {
                Response response = null;
                /* Oauth Variables */
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");

                String json = gson.toJson(params[0]);
                String bodySr = json;
                EpaLog.e(TAG, " bodySr " + bodySr);
                RequestBody body = RequestBody.create(mediaType, bodySr);

                Request request = new Request.Builder()
                        .url(WebUtils.URL + WebUtils.GETSERVICEPROVIDERDETAILS)
                        .post(body).build();

                try {
                    response = client.newCall(request).execute();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                EpaLog.e(TAG, "URl-- " + WebUtils.URL + WebUtils.GETSERVICEPROVIDERDETAILS);

                return response;
            }

            @Override
            protected void onPostExecute(Response response) {
                super.onPostExecute(response);
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    int statusCode = response.code();
                    EpaLog.e("rary", " statusCode " + statusCode);
                    if (statusCode == 200) {

                        String responseBody = response.body().string();
                        EpaLog.e("rary", " responseBody " + responseBody);
                        Reader reader = new InputStreamReader(Utils.getInputstream(responseBody));

                        Serviceproviderdetail detailsResponse = gson.fromJson(reader, Serviceproviderdetail.class);
                        setValuesToFields(detailsResponse);
                    } else {
                        // Give alert to user
                        Utils.showToast(context, "Please try again...");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.execute(new ServiceProviderDetailsRequest(providerId, Utils.LATITUDE, Utils.LONGITUDE));

    }

    private void setValuesToFields(Serviceproviderdetail data) {

        if (data.getProviderName() != null && data.getProviderName().length() > 0) {
            tv_name.setText(data.getProviderName().replace("amp; ", ""));
        } else {
            tv_name.setText(" -----");
        }

        if (data.getProviderSitelink() != null && data.getProviderSitelink().length() > 0) {
            final SpannableString websiteLink = new SpannableString(data.getProviderSitelink());
            websiteLink.setSpan(new UnderlineSpan(), 0, websiteLink.length(), 0);
            tv_website_link.setText(websiteLink);
        } else {
            tv_website_link.setText("");
        }
        if (data.getProviderAddress() != null) {
            String after = data.getProviderAddress().trim().replaceAll(" +", " ");
            SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
            SpannableString address_details = new SpannableString(after);
            address_details.setSpan(new UnderlineSpan(), 0, address_details.length(), 0);
            stringBuilder.append(address_details);

            SpannableString address_details2 = new SpannableString(data.getProviderAddressDetails());
            address_details2.setSpan(new UnderlineSpan(), 0, address_details2.length(), 0);
            stringBuilder.append("\n");
            stringBuilder.append(address_details2);

            //   String fullAdd = address_details + "" + address_details2;
            tv_address_details.setText(stringBuilder);

            int textSize = data.getProviderAddress().length();
            EpaLog.e(TAG, "textSize--- " + textSize);
            if (textSize > 35) {
                //  vv_address.setVisibility(View.VISIBLE);
            }
        } else {
            tv_address_details.setText(" -----");
        }

        if (data.getProviderContactno() != null && data.getProviderContactno().length() > 0) {
            SpannableString phone_number = new SpannableString(data.getProviderContactno());
            phone_number.setSpan(new UnderlineSpan(), 0, phone_number.length(), 0);
            tv_phone_details.setText(phone_number);
        } else {
            tv_phone_details.setText(" -----");
        }

        if (data.getProvider24hourcontact() != null && data.getProvider24hourcontact().length() > 0) {
            ll_24_contact.setVisibility(View.VISIBLE);
            SpannableString phone_number_24 = new SpannableString(data.getProvider24hourcontact());
            phone_number_24.setSpan(new UnderlineSpan(), 0, phone_number_24.length(), 0);
            tv_24_phone_details.setText(phone_number_24);
        } else {
            tv_24_phone_details.setText(" -----");
        }

        if (data.getProviderMailid() != null && data.getProviderMailid().length() > 0) {
            SpannableString email = new SpannableString(data.getProviderMailid());
            email.setSpan(new UnderlineSpan(), 0, email.length(), 0);
            tv_email_details.setText(email);
        } else {
            tv_email_details.setText(" -----");
        }

        if (data.getBrandNames() != null && data.getBrandNames().length() > 0) {
//            SpannableString oem_delaer = new SpannableString(data.getBrandNames());
//            oem_delaer.setSpan(new UnderlineSpan(), 0, oem_delaer.length(), 0);
            ll_oem_dealer.setVisibility(View.VISIBLE);
            tv_oem_dealer_details.setText(data.getBrandNames());
        } else {
            tv_type.setText(" -----");
        }

        if (data.getProviderType() != null && data.getProviderType().length() > 0) {
            tv_type.setText(data.getProviderType());
//            if (data.getProviderType().contains(Utils.OEM_DEALER)) {
//                tv_name.setTextColor(Color.DKGRAY);
//                tv_type.setTextColor(Color.DKGRAY);
//                tv_website_link.setTextColor(Color.DKGRAY);
//            }

        } else {
            tv_type.setText(" -----");
        }

        if (data.getHeaderImage() != null) {
            EpaLog.e(TAG, "getHeaderImage--- " + data.getHeaderImage());
            loadImageFromUrl(ic_header, data.getHeaderImage());

        }

        try {
            EpaLog.e(TAG, "1-- " + data.getProviderLat());
            EpaLog.e(TAG, "2-- " + data.getProviderLon());
            locationData = new LatLng(Double.parseDouble(data.getProviderLat()), Double.parseDouble(data.getProviderLon()));
            EpaLog.e(TAG, "locationData-- " + locationData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadImageFromUrl(final ImageView imgDisplay, final String imageUrl) {
        Picasso.with(context)
                .load(imageUrl)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Log.e("", "The image was obtained correctly" + imageUrl);
                        imgDisplay.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        Log.e("", "The image was not obtained");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        Log.e("", "Getting ready to get the image");
                        //Here you should place a loading gif in the ImageView to
                        //while image is being obtained.
                    }
                });
    }

}
