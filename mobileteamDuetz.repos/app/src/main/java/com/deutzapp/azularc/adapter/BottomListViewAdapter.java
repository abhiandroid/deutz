package com.deutzapp.azularc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deutzapp.R;

import java.util.List;

import com.deutzapp.azularc.data.BottomListViewData;


public class BottomListViewAdapter extends
        RecyclerView.Adapter<BottomListViewAdapter.PostingViewHolder> {

    private Context context;
    private List<BottomListViewData> bottomListViewDataList;

    public BottomListViewAdapter(List<BottomListViewData> bottomListViewDataList) {
        this.bottomListViewDataList = bottomListViewDataList;
    }

    @Override
    public int getItemCount() {
        return bottomListViewDataList.size();
    }

    @Override
    public void onBindViewHolder(final PostingViewHolder postingViewHolder, final int j) {
        BottomListViewData bottomListViewData = bottomListViewDataList.get(postingViewHolder.getAdapterPosition());

        postingViewHolder.tv_name.setText(bottomListViewData.getName());
        postingViewHolder.tv_type.setText(bottomListViewData.getType());
        postingViewHolder.tv_address.setText(bottomListViewData.getAddress());
        if (bottomListViewData.getColor().equals("Red"))
            postingViewHolder.v_color.setBackgroundColor(Color.RED);

        if (bottomListViewData.getColor().equals("Black"))
            postingViewHolder.v_color.setBackgroundColor(Color.DKGRAY);

        if (bottomListViewData.getColor().equals("Gray"))
            postingViewHolder.v_color.setBackgroundColor(Color.LTGRAY);

//        if (bottomListViewData.getColor().equals("White")) {
//            postingViewHolder.v_color.setBackgroundColor(Color.LTGRAY);
//            postingViewHolder.v_color.setBackgroundResource(R.drawable.color_border);
//        }

    }

    @Override
    public PostingViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.ll_bottom_listview_content, viewGroup, false);

        this.context = viewGroup.getContext();
        return new PostingViewHolder(itemView);
    }


    static class PostingViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_type;
        private TextView tv_address;
        View v_color;
        ImageView iv_24_icon;

        PostingViewHolder(View v) {
            super(v);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            tv_type = (TextView) v.findViewById(R.id.tv_type);
            tv_address = (TextView) v.findViewById(R.id.tv_address);
            v_color = (View) v.findViewById(R.id.v_color);
            iv_24_icon = (ImageView) v.findViewById(R.id.iv_24_icon);

        }
    }

}
