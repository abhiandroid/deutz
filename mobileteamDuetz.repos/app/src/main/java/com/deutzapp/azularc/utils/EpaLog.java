package com.deutzapp.azularc.utils;

import android.util.Log;

/**
 * Created by Azularc on 10-02-2016.
 */
public class EpaLog {

    public static String TAG = "DeutzApp";

    public static boolean DEBUG_STATE = true;

    public static void d(String key, String val){
        if(DEBUG_STATE)
        Log.d(key,val);
    }

    public static void e(String key, String val){
        if(DEBUG_STATE)
        Log.e(key,val);
    }
}
