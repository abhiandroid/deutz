package com.deutzapp.azularc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by NabeelRangari on 2/2/17.
 */

public class BottomListViewData implements Serializable {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("color")
    @Expose
    private String color;

    public BottomListViewData(String name, String type, String address, String color) {
        this.name = name;
        this.type = type;
        this.address = address;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "BottomListViewData{" +
                "name=" + name +
                ", type=" + type +
                ", address=" + address +
                ", color=" + color +
                '}';
    }
}
