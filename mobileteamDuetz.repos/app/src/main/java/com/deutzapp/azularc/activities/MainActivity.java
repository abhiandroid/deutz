package com.deutzapp.azularc.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deutzapp.R;
import com.deutzapp.azularc.adapter.BottomListViewAdapterNew;
import com.deutzapp.azularc.data.Brand;
import com.deutzapp.azularc.data.BrandList;
import com.deutzapp.azularc.data.ServiceProviderRequest;
import com.deutzapp.azularc.data.ServiceProviderResponseList;
import com.deutzapp.azularc.data.Serviceproviderlist;
import com.deutzapp.azularc.sharedpreference.SharedPreferenceHelper;
import com.deutzapp.azularc.utils.ConnectionDetector;
import com.deutzapp.azularc.utils.EpaLog;
import com.deutzapp.azularc.utils.Utils;
import com.deutzapp.azularc.utils.WebUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        AdapterView.OnItemClickListener,
        ResultCallback<LocationSettingsResult>,
        ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMapLoadedCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraMoveStartedListener {

    private GoogleMap mMap;
    AutoCompleteTextView et_address;
    public Button bt_filter, bt_info, bt_show_list;
    TextView tv_cancel;
    Context context;
    public static final String TAG = "DeutzApp";
    protected String mLastUpdateTime;
    boolean userLocationStatus = false;

    boolean infoScreenStatus = false;
    ImageView bt_cancel, bt_cross;
    CheckBox cb_full_service_dealer, cb_filter_by_oem, cb_authorized_distributor, cb_dont_show;
    Button bt_apply, bt_ok_got_it, bt_show_list_hide;
    TextView s_select_brand, s_icon;
    TextView tv_authorized_distributor, tv_full_service_dealer, tv_oem_full_service_dealer, tv_dont_show, tv_title, tv_content, tv_right_angle;
    View v_view, v_bottom_view;
    LinearLayout ll_filter, ll_info, ll_show_list, ll_pin_selection, ll_go_to_details, ll_dont_show, ll_pin_content, ll_show_info, ll_list_data, ll_background;
    RelativeLayout rl_header, rl_header_info;
    String locationStatus = "";

    BottomListViewAdapterNew bottomListViewAdapter;
    private List<Serviceproviderlist> serviceproviderlistArrayList = new ArrayList<>();
    private List<Serviceproviderlist> serviceproviderlist =new ArrayList<>();

    boolean isUserLocationStatus = false;
    boolean gotUserLoc = false;

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if (!gotUserLoc) {
            Utils.LATITUDE = location.getLatitude() + "";
            Utils.LONGITUDE = location.getLongitude() + "";
            gotUserLoc = true;
        }

        EpaLog.e(TAG, "LATITUDE- " + Utils.LATITUDE + "LONGITUDE- " + Utils.LONGITUDE);
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateLocationUI();
        EpaLog.e(TAG, "2");

    }

    @Override
    public void onMapLoaded() {
        LatLng pos = mMap.getCameraPosition().target;
        Log.e(TAG, "Pos: " + pos.toString());
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({GRANTED, DENIED, BLOCKED})
    public @interface PermissionStatus {
    }

    public static final int GRANTED = 0;
    public static final int DENIED = 1;
    public static final int BLOCKED = 2;

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyCNnIVLGVKleTSUJn8W3gqtMyo_taa6ZCs";

    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000; //1 minute

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;

    /**
     * Represents a geographical location.
     */
    protected Location mCurrentLocation;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    Marker userLocation;
    View mview = null;
    View nview = null;

    Typeface face;
    ListView lv_spinner;
    LinearLayout ll_spinner;
    RecyclerView recView;
    TextView tv_no_data;

    Boolean isInternetPresent = false;
    ConnectionDetector connectionDetector;
    String brandName = "";

    Bitmap bitmap1 = null;
    Bitmap bitmap2 = null;
    Bitmap bitmap3 = null;
    Bitmap bitmap4 = null;

    Bitmap small1 = null;
    Bitmap small2 = null;
    Bitmap small3 = null;
    Bitmap small4 = null;

    Activity activity;
    int markerSize = 100;
    int smallMarkerSize = 25;
    SupportMapFragment mapFragment;
    boolean isMarkerClick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        activity = this;


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (Utils.checkGooglePlayServicesAvailable(MainActivity.this)) {
            mapFragment.getMapAsync(this);

            buildGoogleApiClient();
            createLocationRequest();
            buildLocationSettingsRequest();
        }


        mRequestingLocationUpdates = false;
        connectionDetector = new ConnectionDetector(context);
        et_address = (AutoCompleteTextView) findViewById(R.id.et_address);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        bt_filter = (Button) findViewById(R.id.bt_filter);
        bt_info = (Button) findViewById(R.id.bt_info);
        bt_show_list = (Button) findViewById(R.id.bt_show_list);

        nview = this.getLayoutInflater().inflate(R.layout.activity_filter, null);
        rl_header = (RelativeLayout) nview.findViewById(R.id.rl_header);

        mview = this.getLayoutInflater().inflate(R.layout.activity_info, null);
        rl_header_info = (RelativeLayout) mview.findViewById(R.id.rl_header_info);

        ll_filter = (LinearLayout) findViewById(R.id.ll_filter);
        ll_info = (LinearLayout) findViewById(R.id.ll_info);
        ll_show_list = (LinearLayout) findViewById(R.id.ll_show_list);
        ll_pin_selection = (LinearLayout) findViewById(R.id.ll_pin_selection);
        ll_go_to_details = (LinearLayout) findViewById(R.id.ll_go_to_details);
        ll_dont_show = (LinearLayout) findViewById(R.id.ll_dont_show);
        ll_pin_content = (LinearLayout) findViewById(R.id.ll_pin_content);
        ll_show_info = (LinearLayout) findViewById(R.id.ll_show_info);
        ll_list_data = (LinearLayout) findViewById(R.id.ll_list_data);
        ll_background = (LinearLayout) findViewById(R.id.ll_background);

        v_view = (View) findViewById(R.id.v_view);
        v_bottom_view = findViewById(R.id.v_bottom_view);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_content = (TextView) findViewById(R.id.tv_content);
        tv_right_angle = (TextView) findViewById(R.id.tv_right_angle);

        recView = (RecyclerView) findViewById(R.id.recView);
        tv_no_data = (TextView) findViewById(R.id.tv_no_data);

        face = Typeface.createFromAsset(context.getAssets(),
                "fontawesome-webfont.ttf");

        bt_filter.setTypeface(face);
        bt_filter.setText(R.string.filter);

        bt_info.setTypeface(face);
        bt_info.setText(R.string.info);

        tv_right_angle.setTypeface(face);
        tv_right_angle.setText(R.string.right_angle);
        tv_right_angle.setTextColor(getResources().getColor(R.color.grey_darkest));
        try {
            et_address.setAdapter(new GooglePlacesAutocompleteAdapter(this, android.R.layout.simple_list_item_1));

        } catch (Exception e) {
            e.printStackTrace();
        }

        et_address.setOnItemClickListener(this);
        et_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ll_filter.isShown()) {
                    ll_filter.setVisibility(View.GONE);
                    v_view.setVisibility(View.VISIBLE);
                    ll_show_list.setVisibility(View.VISIBLE);
                } else if (ll_info.isShown()) {
                    ll_info.setVisibility(View.GONE);
                    v_view.setVisibility(View.VISIBLE);
                    ll_show_list.setVisibility(View.VISIBLE);

                }
            }
        });

        et_address.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //   EpaLog.e(TAG, "Touch : ");
                if (!hasFocus) {
                    hideKeyboard(v);
                    // EpaLog.e(TAG, "Touch : 1");
                } else {
                    // EpaLog.e(TAG, "Touch : 2");
                }
            }
        });

        bt_cancel = (ImageView) findViewById(R.id.bt_cancel);
        cb_full_service_dealer = (CheckBox) findViewById(R.id.cb_full_service_dealer);
        cb_authorized_distributor = (CheckBox) findViewById(R.id.cb_authorized_distributor);
        cb_filter_by_oem = (CheckBox) findViewById(R.id.cb_filter_by_oem);
        bt_apply = (Button) findViewById(R.id.bt_apply);
        s_select_brand = (TextView) findViewById(R.id.s_select_brand);
        s_icon = (TextView) findViewById(R.id.s_icon);

        ll_spinner = (LinearLayout) findViewById(R.id.ll_spinner);
        s_icon.setTypeface(face);
        s_icon.setText(R.string.spinner_icon);

        bt_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaLog.e(TAG, "bt_apply--- ");
                try {
                    ll_filter.setVisibility(View.GONE);
                    v_view.setVisibility(View.VISIBLE);
                    ll_show_list.setVisibility(View.VISIBLE);

                    if (Utils.isNetworkAvailable(context)) {
                        if (Utils.LATITUDE != null && Utils.LONGITUDE != null && Utils.LATITUDE.length() > 0 && Utils.LONGITUDE.length() > 0) {
                            getServiceProviderAPI();
                            isUserLocationStatus = true;
                        }
                    } else {
                        Utils.showAlertDialog(context, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cb_full_service_dealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaLog.e(TAG, "CheckBox--- " + cb_full_service_dealer.isChecked());

            }
        });

        cb_authorized_distributor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaLog.e(TAG, "CheckBox--- " + cb_authorized_distributor.isChecked());

            }
        });

        cb_filter_by_oem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaLog.e(TAG, "CheckBox--- " + cb_filter_by_oem.isChecked());

            }
        });

        bt_cancel.setColorFilter(Color.RED);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaLog.e(TAG, "bt_cancel");
                try {
                    ll_filter.setVisibility(View.GONE);
                    v_view.setVisibility(View.VISIBLE);
                    v_bottom_view.setVisibility(View.GONE);
                    ll_show_list.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        nview = this.getLayoutInflater().inflate(R.layout.activity_info, null);
        bt_cross = (ImageView) findViewById(R.id.bt_cross);
        cb_dont_show = (CheckBox) findViewById(R.id.cb_dont_show);
        bt_ok_got_it = (Button) findViewById(R.id.bt_ok_got_it);

        tv_authorized_distributor = (TextView) findViewById(R.id.tv_authorized_distributor);
        tv_full_service_dealer = (TextView) findViewById(R.id.tv_full_service_dealer);
        tv_oem_full_service_dealer = (TextView) findViewById(R.id.tv_oem_full_service_dealer);
      //  tv_oem_dealer = (TextView) findViewById(R.id.tv_oem_dealer);
        tv_dont_show = (TextView) findViewById(R.id.tv_dont_show);


        try {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            String title1 = "-Service provider authorized to conduct all service and warranty repairs on DEUTZ engines.";
            String heading1 = "Authorized Distributor";

            SpannableString blueSpannable = new SpannableString(heading1);
            blueSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, heading1.length(), 0);
            builder.append(blueSpannable);
            builder.append(title1);

            tv_authorized_distributor.setText(builder, TextView.BufferType.SPANNABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            String title2 = "-Service provider authorized to conduct all service and warranty repairs on DEUTZ engines.";
            String heading2 = "Full-Service Dealer (CC3)";

            SpannableString blueSpannable = new SpannableString(heading2);
            blueSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, heading2.length(), 0);
            builder.append(blueSpannable);
            builder.append(title2);

            tv_full_service_dealer.setText(builder, TextView.BufferType.SPANNABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            String title3 = "-Service provider authorized to perform most major engine and after-treatment repairs excluding rebuilds and internals.";
            String heading3 = "Limited-Service Dealer (CC2)";

            SpannableString blueSpannable = new SpannableString(heading3);
            blueSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, heading3.length(), 0);
            builder.append(blueSpannable);
            builder.append(title3);

            tv_oem_full_service_dealer.setText(builder, TextView.BufferType.SPANNABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            SpannableStringBuilder builder = new SpannableStringBuilder();
//            String title4 = " Service provider authorized to conduct any and all service and warranty repairs on DEUTZ engines they are dealer for, with the exception of internal engine repairs.";
//            String heading4 = "OEM Dealer -";
//
//            SpannableString blueSpannable = new SpannableString(heading4);
//            blueSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, heading4.length(), 0);
//            builder.append(blueSpannable);
//            builder.append(title4);
//
//            tv_oem_dealer.setText(builder, TextView.BufferType.SPANNABLE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            String text1 = "Don't show this message again \n";
            String text2 = "(Access from ' " + getResources().getString(R.string.info) + " ' button above)";

            SpannableString blueSpannable = new SpannableString(text2);
            builder.append(text1);
            builder.append(blueSpannable);

            tv_dont_show.setTypeface(face);
            tv_dont_show.setText(builder, TextView.BufferType.SPANNABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        bt_cross.setColorFilter(Color.RED);
        bt_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaLog.e(TAG, "bt_cancel");
                try {
                    ll_info.setVisibility(View.GONE);
                    v_view.setVisibility(View.VISIBLE);
                    ll_show_list.setVisibility(View.VISIBLE);
                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.DONT_SHOW, "false", context);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        bt_ok_got_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_info.setVisibility(View.GONE);
                v_view.setVisibility(View.VISIBLE);
                ll_show_list.setVisibility(View.VISIBLE);
                if (cb_dont_show.isChecked()) {
                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.DONT_SHOW, "true", context);
                } else {
                    SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.DONT_SHOW, "false", context);
                }
            }
        });

        ll_filter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // ignore all touch events
                return true;
            }
        });

        ll_info.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // ignore all touch events
                return true;
            }
        });

        rl_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
            }
        });

        rl_header_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
            }
        });

        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "Roboto-Regular.ttf");

        bt_show_list.setTypeface(face);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        isInternetPresent = connectionDetector.isConnectingToInternet();
        EpaLog.e(TAG, "isInternetPresent- " + isInternetPresent);
        if (Utils.isNetworkAvailable(context)) {
            getBrandList();
        } else {
            Utils.showAlertDialog(context, "No Internet Connection",
                    "You don't have internet connection.", false);
        }


        float screenSize = Utils.determineScreenDensity(activity);
        EpaLog.e(TAG, "screenSize--- " + screenSize);

        if (screenSize <= 1.5) {
            markerSize = 85;
            smallMarkerSize = 25;
        }

        if (screenSize == 2.0) {
            markerSize = 100;
            smallMarkerSize = 35;

        }

        if (screenSize == 3.0) {
            markerSize = 125;
            smallMarkerSize = 40;
        }

        if (screenSize == 4.0) {
            markerSize = 150;
            smallMarkerSize = 55;
        }

        bitmap1 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.marker11);
        bitmap1 = Utils.getResizedBitmap(bitmap1, markerSize, markerSize);

        bitmap2 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.marker22);
        bitmap2 = Utils.getResizedBitmap(bitmap2, markerSize, markerSize);

        bitmap3 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.marker33);
        bitmap3 = Utils.getResizedBitmap(bitmap3, markerSize, markerSize);

        bitmap4 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.marker44);
        bitmap4 = Utils.getResizedBitmap(bitmap4, markerSize, markerSize);




        small1 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.small1);
        small1 = Utils.getResizedBitmap(small1, smallMarkerSize, smallMarkerSize);

        small2 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.small2);
        small2 = Utils.getResizedBitmap(small2, smallMarkerSize, smallMarkerSize);

        small3 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.small3);
        small3 = Utils.getResizedBitmap(small3, smallMarkerSize, smallMarkerSize);

        small4 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.small4);
        small4 = Utils.getResizedBitmap(small4, smallMarkerSize, smallMarkerSize);




    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EpaLog.e(TAG, "onDestroy");
        // Utils.LATITUDE = "";
        // Utils.LONGITUDE = "";
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        checkLocationSettings();
    }

    @Override
    protected void onResume() {
        super.onResume();

        et_address.clearFocus();
        if (getPermissionStatus(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == BLOCKED || getPermissionStatus(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == DENIED) {
            if (Build.VERSION.SDK_INT >= 23 &&
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);
                //EpaLog.e(TAG, "Permission");
            }
        }

        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.

        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();


        }
        ll_pin_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getId = userLocation.getTitle();
                String[] splitString = getId.split("%");
                String ID = splitString[1];
                tv_title.setText(ID);
                //   String id = getProviderId(userLocation.getTitle());
                EpaLog.e(TAG, "id---" + ID);
                Utils.LATITUDE = Utils.getStringFromDouble(userLocation.getPosition().latitude);
                Utils.LONGITUDE = Utils.getStringFromDouble(userLocation.getPosition().longitude);
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra(Utils.PROVIDERID, ID); // PROVIDER ID
                startActivity(intent);
                ll_pin_selection.setVisibility(View.GONE);
                v_view.setVisibility(View.VISIBLE);
                try {
                    onMapClick(userLocation.getPosition());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ll_go_to_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getId = userLocation.getTitle();
                String[] splitString = getId.split("%");
                String ID = splitString[1];
                tv_title.setText(ID);
                //  String id = getProviderId(userLocation.getTitle());
                EpaLog.e(TAG, "id---" + ID);
                Utils.LATITUDE = Utils.getStringFromDouble(userLocation.getPosition().latitude);
                Utils.LONGITUDE = Utils.getStringFromDouble(userLocation.getPosition().longitude);
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra(Utils.PROVIDERID, ID); // PROVIDER ID
                startActivity(intent);
                ll_pin_selection.setVisibility(View.GONE);
                v_view.setVisibility(View.VISIBLE);
                try {
                    onMapClick(userLocation.getPosition());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cb_filter_by_oem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb_filter_by_oem.isChecked()) {
                    ll_spinner.setVisibility(View.VISIBLE);
                } else {
                    ll_spinner.setVisibility(View.INVISIBLE);
                }
            }
        });

        if (v_view.isShown()) {
            ll_info.setVisibility(View.GONE);
            ll_filter.setVisibility(View.GONE);
            EpaLog.e(TAG, "v_view 1");
        }

        bt_show_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (serviceproviderlistArrayList != null && serviceproviderlistArrayList.size() > 0) {
                    String buttonText = bt_show_list.getText().toString();
                    if (buttonText.contains("SHOW LIST")) {
                        ll_list_data.setVisibility(View.VISIBLE);
                        v_bottom_view.setVisibility(View.VISIBLE);
                        v_view.setVisibility(View.GONE);
                        bt_show_list.setText("HIDE LIST");
                        bt_show_list.setBackgroundColor(Color.DKGRAY);
                        EpaLog.e(TAG, "SHOW LIST--");
                        if (ll_pin_selection.isShown()) {
                            EpaLog.e(TAG, "ll_pin_selection 7");
                            ll_pin_selection.setVisibility(View.GONE);
                        }
                    }
                    if (buttonText.contains("HIDE LIST")) {
                        ll_list_data.setVisibility(View.GONE);
                        v_bottom_view.setVisibility(View.GONE);
                        v_view.setVisibility(View.VISIBLE);
                        bt_show_list.setText("SHOW LIST");
                        bt_show_list.setBackgroundColor(Color.RED);
                        EpaLog.e(TAG, "HIDE LIST--");
                        if (!ll_pin_selection.isShown() && isMarkerClick) {
                            EpaLog.e(TAG, "ll_pin_selection 8");
                            ll_pin_selection.setVisibility(View.VISIBLE);
                            v_view.setVisibility(View.GONE);
                        }
                    }

                    if (v_view.isShown()) {
                        EpaLog.e(TAG, "v_view 1");
                    }

                    if (ll_info.isShown()) {
                        EpaLog.e(TAG, "ll_info 2");
                    }

                    if (ll_filter.isShown()) {
                        EpaLog.e(TAG, "ll_filter 3");
                    }

                    if (v_bottom_view.isShown()) {
                        EpaLog.e(TAG, "v_bottom_view 4");
                    }

                    if (ll_list_data.isShown()) {
                        EpaLog.e(TAG, "ll_list_data 5");
                    }

                    if (ll_show_list.isShown()) {
                        EpaLog.e(TAG, "ll_show_list 6");
                    }

                   /* if (ll_pin_selection.isShown()) {
                        EpaLog.e(TAG, "ll_pin_selection 7");
                        ll_pin_selection.setVisibility(View.GONE);
                    }*/
                } else {
                    Utils.showToast(context, "No providers found!");
                }

//                try {
//                    onMapClick(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

            }
        });

        if (v_view.isShown()) {
            EpaLog.e(TAG, "v_view 1");
        }

        if (ll_info.isShown()) {
            EpaLog.e(TAG, "ll_info 2");
        }

        if (ll_filter.isShown()) {
            EpaLog.e(TAG, "ll_filter 3");
        }

        if (v_bottom_view.isShown()) {
            EpaLog.e(TAG, "v_bottom_view 4");
        }

        if (ll_list_data.isShown()) {
            EpaLog.e(TAG, "ll_list_data 5");
        }

        if (ll_show_list.isShown()) {
            EpaLog.e(TAG, "ll_show_list 6");
        }

        if (ll_pin_selection.isShown()) {
            EpaLog.e(TAG, "ll_pin_selection 7");
            // ll_pin_selection.setVisibility(View.GONE);
        }

    }

    public void showInfoScreen() {
        String dont_show_Status = SharedPreferenceHelper.getPreference(context, SharedPreferenceHelper.DONT_SHOW);
        EpaLog.e(TAG, "dont_show_Status-- " + dont_show_Status);
        if (dont_show_Status == null || dont_show_Status.equals("")) {
            // EpaLog.e(TAG, "dont_show_Status--1 ");
            bt_info.performClick();
        }
        if (dont_show_Status != null && dont_show_Status.contains("false")) {
            //EpaLog.e(TAG, "dont_show_Status--2 " + dont_show_Status);
            bt_info.performClick();
        }
//
//        if (dont_show_Status != null && dont_show_Status.equals("true")){
//
//        }

    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = "";
        try {
            str = (String) adapterView.getItemAtPosition(position);
            ll_pin_selection.setVisibility(View.GONE);
            v_view.setVisibility(View.VISIBLE);
            ll_show_list.setVisibility(View.VISIBLE);
            v_bottom_view.setVisibility(View.GONE);
            ll_list_data.setVisibility(View.GONE);
            bt_show_list.setText("SHOW LIST");
            bt_show_list.setBackgroundColor(Color.RED);

        } catch (Exception e) {
            e.printStackTrace();
        }

        LatLng searchResult = null;
        try {
            searchResult = getLocationFromAddress(context, str);
            EpaLog.e(TAG, "searchResult : " + searchResult.toString() + "");
            et_address.setSelection(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        et_address.clearFocus();
        hideKeyboard(view);

        if (str != null) {
            try {
                if (searchResult != null) {

                    EpaLog.e(TAG, " searchResult.latitude---" + searchResult.latitude);
                    EpaLog.e(TAG, " searchResult.longitude---" + searchResult.longitude);

                    Utils.LATITUDE = Utils.getStringFromDouble(searchResult.latitude);
                    Utils.LONGITUDE = Utils.getStringFromDouble(searchResult.longitude);

                    EpaLog.e(TAG, " Utils.LATITUDE---" + Utils.LATITUDE);
                    EpaLog.e(TAG, " Utils.LONGITUDE---" + Utils.LONGITUDE);

                    ll_pin_selection.setVisibility(View.GONE);
                }
                getServiceProviderAPI();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchResult, 11f));
        }

    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

            }

        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }


    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the com.deutzapp.activities.data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                    if (results != null && results.count > 0) {
                        resultList = (ArrayList<String>) results.values;
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }


    public void showList(View view) {

    }


    public void filter(View view) {
        hideKeyboard(view);
        try {
            onMapClick(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ll_info.isShown()) {
            ll_info.setVisibility(View.GONE);
            ll_filter.setVisibility(View.VISIBLE);
            v_view.setVisibility(View.GONE);
            v_bottom_view.setVisibility(View.GONE);

            bt_show_list.setText("SHOW LIST");
            bt_show_list.setBackgroundColor(Color.RED);

            ll_list_data.setVisibility(View.GONE);
            ll_show_list.setVisibility(View.GONE);
        } else if (ll_filter.isShown()) {
            ll_filter.setVisibility(View.GONE);
            v_view.setVisibility(View.VISIBLE);
            ll_show_list.setVisibility(View.VISIBLE);

        } else {
            ll_filter.setVisibility(View.VISIBLE);
            v_view.setVisibility(View.GONE);
            v_bottom_view.setVisibility(View.GONE);
            bt_show_list.setText("SHOW LIST");
            bt_show_list.setBackgroundColor(Color.RED);
            ll_list_data.setVisibility(View.GONE);
            ll_show_list.setVisibility(View.GONE);
            ll_pin_selection.setVisibility(View.GONE);
        }

    }

    public void cancel(View view) {
        hideKeyboard(view);
        et_address.setText("");

    }

    public void info(View view) {
        hideKeyboard(view);
        //EpaLog.e(TAG, "info----");
        try {
            onMapClick(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ll_filter.isShown()) {
            ll_filter.setVisibility(View.GONE);
            ll_info.setVisibility(View.VISIBLE);
            v_view.setVisibility(View.GONE);
            v_bottom_view.setVisibility(View.GONE);
            bt_show_list.setText("SHOW LIST");
            bt_show_list.setBackgroundColor(Color.RED);
            ll_show_list.setVisibility(View.GONE);
            ll_list_data.setVisibility(View.GONE);
            if (infoScreenStatus) {
                ll_dont_show.setVisibility(View.GONE);
                bt_ok_got_it.setVisibility(View.GONE);
            }
            // EpaLog.e("info", "info-1");
        } else if (ll_info.isShown()) {
            ll_info.setVisibility(View.GONE);
            v_view.setVisibility(View.VISIBLE);
            ll_show_list.setVisibility(View.VISIBLE);
            EpaLog.e("info", "info-2");

        } else {
            ll_info.setVisibility(View.VISIBLE);
            v_view.setVisibility(View.GONE);
            v_bottom_view.setVisibility(View.GONE);
            bt_show_list.setText("SHOW LIST");
            bt_show_list.setBackgroundColor(Color.RED);
            ll_show_list.setVisibility(View.GONE);
            ll_list_data.setVisibility(View.GONE);
            ll_pin_selection.setVisibility(View.GONE);
            if (infoScreenStatus) {
                ll_dont_show.setVisibility(View.GONE);
                bt_ok_got_it.setVisibility(View.GONE);
            }
            // EpaLog.e("info", "info-3");
        }

    }

    @PermissionStatus
    public static int getPermissionStatus(Activity activity, String androidPermissionName) {
        if (ContextCompat.checkSelfPermission(activity, androidPermissionName) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, androidPermissionName)) {
                return BLOCKED;
            }
            return DENIED;
        }
        return GRANTED;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        EpaLog.e(TAG, "onBackPressed");
        try {
            if (ll_filter.isShown()) {
                ll_filter.setVisibility(View.GONE);
                v_view.setVisibility(View.VISIBLE);
                v_bottom_view.setVisibility(View.GONE);
                ll_show_list.setVisibility(View.VISIBLE);
                // EpaLog.e(TAG, "onBackPressed 1");
            } else if (ll_info.isShown()) {
                ll_info.setVisibility(View.GONE);
                v_view.setVisibility(View.VISIBLE);
                v_bottom_view.setVisibility(View.GONE);
                ll_show_list.setVisibility(View.VISIBLE);
                SharedPreferenceHelper.savePreferences(SharedPreferenceHelper.DONT_SHOW, "false", context);
                // EpaLog.e(TAG, "onBackPressed 2");
            } else if (ll_list_data.isShown()) {
                ll_list_data.setVisibility(View.GONE);
                v_view.setVisibility(View.VISIBLE);
                v_bottom_view.setVisibility(View.GONE);
                bt_show_list.setText("SHOW LIST");
                bt_show_list.setBackgroundColor(Color.RED);
                // EpaLog.e(TAG, "onBackPressed 2");
            } else if (!ll_filter.isShown() && !ll_info.isShown()) {
                //  EpaLog.e(TAG, "onBackPressed 3");
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates fields based on com.deutzapp.activities.data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
            updateUI();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.e(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                locationStatus = "success";
                userLocationStatus = true;
                try {
                    showInfoScreen();
                    infoScreenStatus = true;
                    //addMarkersToMap();
                    //getServiceProviderAPI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.e(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                locationStatus = "fail";
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.e(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                locationStatus = "fail";
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        userLocationStatus = true;
                        try {
                            showInfoScreen();
                            infoScreenStatus = true;
                            //addMarkersToMap();
                            //getServiceProviderAPI();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        userLocationStatus = false;
                        try {
                            showInfoScreen();
                            infoScreenStatus = true;
                            //addMarkersToMap();
                            // getServiceProviderAPI();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //  Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest, this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
            }
        });

    }

    /**
     * Updates all UI fields.
     */
    private void updateUI() {
        updateLocationUI();
        //EpaLog.e(TAG, "1");
    }

    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            EpaLog.e(TAG, "Lat : " + mCurrentLocation.getLatitude() + "Lng : " + mCurrentLocation.getLongitude() + "Time : " + mLastUpdateTime);

            // Utils.LATITUDE = mCurrentLocation.getLatitude() + "";
            // Utils.LONGITUDE = mCurrentLocation.getLongitude() + "";
            EpaLog.e("Utils", "Lat : " + Utils.LATITUDE + "Lng : " + Utils.LONGITUDE + "Time : " + mLastUpdateTime);

            if (!isUserLocationStatus) {
                if (Utils.isNetworkAvailable(context)) {
                    if (Utils.LATITUDE != null && Utils.LONGITUDE != null && Utils.LATITUDE.length() > 0 && Utils.LONGITUDE.length() > 0) {
                        getServiceProviderAPI();
                        isUserLocationStatus = true;
                    }
                }

            }
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {

        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, (LocationListener) this

        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
        //   EpaLog.e(TAG, "onStop");
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        // If the initial location was never previously requested, we use
        // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
        // its value in the Bundle and check for it in onCreate(). We
        // do not request it again unless the user specifically requests location updates by pressing
        // the Start Updates button.
        //
        // Because we cache the value of the initial location in the Bundle, it means that if the
        // user launches the activity,
        // moves to a new location, and then changes the device orientation, the original location
        // is displayed as the activity is re-created.
        if (userLocationStatus) {
            if (mCurrentLocation == null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                updateLocationUI();
                EpaLog.e(TAG, "3");
            }
        } else {
            //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)), 4.5f));
        }

    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity com.deutzapp.activities.data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

            EpaLog.e(TAG, "p1 : " + p1.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return p1;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // Convert a view to bitmap
//    public static Bitmap createDrawableFromView(Context context, View view) {
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
//        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
//        view.buildDrawingCache();
//        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//
//        Canvas canvas = new Canvas(bitmap);
//        view.draw(canvas);
//
//        return bitmap;
//    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        isMarkerClick = true;
        EpaLog.e(TAG, "onMarkerClick");
        ll_pin_selection.setVisibility(View.VISIBLE);
        v_view.setVisibility(View.GONE);

        if (v_bottom_view.isShown()) {
            v_bottom_view.setVisibility(View.GONE);
        }
        if (ll_list_data.isShown()) {
            ll_list_data.setVisibility(View.GONE);
            bt_show_list.setText("SHOW LIST");
            bt_show_list.setBackgroundColor(Color.RED);

        }
        String getTitle = "";
        try {
            if (null != userLocation) {

                if (userLocation.getSnippet().equals(Utils.AUTHORIZED_DISTRIBUTOR)) {
                    EpaLog.e(TAG, "0");

                    getTitle = userLocation.getTitle();
                    String[] splitString = getTitle.split("%");
                    String title = splitString[0];
                    tv_title.setText(title);
                    tv_title.setText(userLocation.getTitle());
                    tv_content.setText(userLocation.getSnippet());

                    userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small1));
                }

                if (userLocation.getSnippet().equals(Utils.FULL_SERVICE_DEALER)) {
                    EpaLog.e(TAG, "1");
                    getTitle = userLocation.getTitle();
                    String[] splitString = getTitle.split("%");
                    String title = splitString[0];
                    tv_title.setText(title);
                    tv_content.setText(userLocation.getSnippet());

                    userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small2));

                }

                if (userLocation.getSnippet().equals(Utils.OEM_FULL_SERVICE_DEALER)) {
                    EpaLog.e(TAG, "2");
                    getTitle = userLocation.getTitle();
                    String[] splitString = getTitle.split("%");
                    String title = splitString[0];
                    tv_title.setText(title);
                    tv_content.setText(userLocation.getSnippet());

                    userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small3));
                }

//                if (userLocation.getSnippet().equals(Utils.OEM_DEALER)) {
//                    EpaLog.e(TAG, "3");
//                    getTitle = userLocation.getTitle();
//                    String[] splitString = getTitle.split("%");
//                    String title = splitString[0];
//                    tv_title.setText(title);
//                    tv_content.setText(userLocation.getSnippet());
//
//                    userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small4));
//                }

            }

            if (marker.getSnippet().equals(Utils.AUTHORIZED_DISTRIBUTOR)) {
                EpaLog.e(TAG, "00");
                userLocation = marker;

                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap1));

                getTitle = userLocation.getTitle();
                String[] splitString = getTitle.split("%");
                String title = splitString[0];
                tv_title.setText(title);
                tv_content.setText(marker.getSnippet());

            }

            if (marker.getSnippet().equals(Utils.FULL_SERVICE_DEALER)) {
                EpaLog.e(TAG, "11");
                userLocation = marker;

                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap2));

                getTitle = userLocation.getTitle();
                String[] splitString = getTitle.split("%");
                String title = splitString[0];
                tv_title.setText(title);
                tv_content.setText(marker.getSnippet());
            }

            if (marker.getSnippet().equals(Utils.OEM_FULL_SERVICE_DEALER)) {
                EpaLog.e(TAG, "22");
                userLocation = marker;

                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap3));

                getTitle = userLocation.getTitle();
                String[] splitString = getTitle.split("%");
                String title = splitString[0];
                tv_title.setText(title);
                tv_content.setText(marker.getSnippet());
            }


//            if (marker.getSnippet().equals(Utils.OEM_DEALER)) {
//                EpaLog.e(TAG, "33");
//                userLocation = marker;
//
//                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap4));
//
//                getTitle = userLocation.getTitle();
//                String[] splitString = getTitle.split("%");
//                String title = splitString[0];
//                tv_title.setText(title);
//                tv_content.setText(marker.getSnippet());
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;

    }

    @Override
    public void onMapClick(LatLng latLng) {
        EpaLog.e(TAG, "onMapClick");
        isMarkerClick = false;
        if (ll_pin_selection.isShown()) {
            ll_pin_selection.setVisibility(View.GONE);
            EpaLog.e(TAG, "0---");
        }
        if (ll_info.isShown()) {
            EpaLog.e(TAG, "1---");
        }
        if (ll_filter.isShown()) {
            EpaLog.e(TAG, "2---");
        } else if (!ll_filter.isShown() && !ll_info.isShown() && !ll_list_data.isShown()) {
            v_view.setVisibility(View.VISIBLE);
            EpaLog.e(TAG, "3---");
        }

        if (null != userLocation) {
            if (userLocation.getSnippet().equals(Utils.AUTHORIZED_DISTRIBUTOR)) {
                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small1));

            }
            if (userLocation.getSnippet().equals(Utils.FULL_SERVICE_DEALER)) {
                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small2));

            }
            if (userLocation.getSnippet().equals(Utils.OEM_FULL_SERVICE_DEALER)) {
                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small3));
            }
//            if (userLocation.getSnippet().equals(Utils.OEM_DEALER)) {
//                userLocation.setIcon(BitmapDescriptorFactory.fromBitmap(small4));
//
//            }

        }
        userLocation = null;
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            EpaLog.e("Gesture", "1");
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            EpaLog.e("Gesture", "2");
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            EpaLog.e("Gesture", "3");
        }
    }

    @Override
    public void onCameraMove() {
        EpaLog.e("Gesture", "onCameraMove");
    }

    @Override
    public void onCameraMoveCanceled() {
        EpaLog.e("Gesture", "onCameraMoveCanceled");
    }

    @Override
    public void onCameraIdle() {
        LatLng pos = mMap.getCameraPosition().target;
        if (pos != null) {
            getServiceProviderList(pos);
            Log.e(TAG, "Pos: " + pos.toString());
        }
        EpaLog.e("Gesture", "onCameraIdle");
    }


    @SuppressLint("StaticFieldLeak")
    private void getBrandList() {
        new AsyncTask<Void, Void, Response>() {
            List<Brand> brandDataList;
            HashMap<Integer, String> hashMapInd = new HashMap<>();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Response doInBackground(Void... params) {
                Response response = null;
                /* Oauth Variables */
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .get()
                        .url(WebUtils.MAIN_URL + WebUtils.BRAND)
                        .build();

                try {
                    response = client.newCall(request).execute();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return response;
            }

            @Override
            protected void onPostExecute(Response response) {
                super.onPostExecute(response);

                try {
                    int statusCode = response.code();
                    EpaLog.e(TAG, " statusCode " + statusCode);
                    if (statusCode == 200) {

                        Gson gson = new Gson();
                        String responseBody = response.body().string();
                        Reader reader = new InputStreamReader(Utils.getInputstream(responseBody));

                        BrandList brandList = gson.fromJson(reader, BrandList.class);

                        brandDataList = brandList.getBrands();

                        for (Brand objInd : brandDataList) {
                            hashMapInd.put(Utils.getIntegerFromString(objInd.getBrandId()), objInd.getBrandName());
                        }
                             EpaLog.e(TAG, " brandDataList " + brandDataList.toString());

                        ArrayAdapter<Brand> dataAdapter = new BrandSpinnerAdapter(context, R.layout.list_item_spinner, brandDataList.toArray(new Brand[brandDataList.size()]));

                        final Dialog spinnerDialog = new Dialog(context);
                        spinnerDialog.setContentView(R.layout.ll_spinner);

                        lv_spinner = (ListView) spinnerDialog.findViewById(R.id.lv_spinner);
                        lv_spinner.setAdapter(dataAdapter);
                        ll_spinner.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                spinnerDialog.show();
                            }
                        });

                        lv_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                Brand model = brandDataList.get(i);
                                s_select_brand.setText(model.getBrandName());
                                brandName = model.getBrandId();
                                Log.e(TAG,"BrandId-- "+brandName);
                                spinnerDialog.dismiss();
                            }
                        });

                    } else {
                        // Give alert to user
                        Utils.showToast(context, "Please try again...");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.execute();

    }

    public class BrandSpinnerAdapter extends ArrayAdapter<Brand> {
        private Context context;
        // Your custom values for the spinner (User)
        public Brand[] values;

        public BrandSpinnerAdapter(Context context, int textViewResourceId,
                                   Brand[] values) {
            super(context, textViewResourceId, values);
            this.context = context;
            this.values = values;
        }

        public int getCount() {
            return values.length;
        }

        public Brand getItem(int position) {
            return values[position];
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            // Spinner Drop down elements

            View mySpinner = inflater.inflate(R.layout.list_item_spinner, parent, false);
            final TextView main_text = (TextView) mySpinner.findViewById(R.id.textView);

            main_text.setText(values[position].getBrandName());

            EpaLog.e(TAG, "Adapter---------------");


            return mySpinner;
        }

    }

    @SuppressLint("StaticFieldLeak")
    private void getServiceProviderAPI() {
        new AsyncTask<ServiceProviderRequest, Void, ServiceProviderResponseList>() {

            ProgressDialog progressDialog;
            Response response;
            int statusCode = 0;
            Gson gson = new Gson();
            String responseBody = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    progressDialog = Utils.showProgressDialig(Utils.PLEASE_WAIT, Utils.FETCHING_DETAILS, false, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected ServiceProviderResponseList doInBackground(ServiceProviderRequest... serviceProviderRequests) {

                OkHttpClient client = new OkHttpClient();
                Gson gson = new Gson();
                String stringBody = gson.toJson(serviceProviderRequests[0]);
                EpaLog.e(TAG, "stringBody-- " + stringBody);

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, stringBody);
                Request request = new Request.Builder()
                        .url(WebUtils.URL + WebUtils.GETSERVICEPROVIDERS)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .build();

                EpaLog.e(TAG, "Providers -- " + WebUtils.GETSERVICEPROVIDERS);


                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ServiceProviderResponseList serviceProviderResponseList) {
                super.onPostExecute(serviceProviderResponseList);

                try {

                    statusCode = response.code();
                    mMap.clear();
                    isMarkerClick = false;
                    userLocation = null;
                    serviceproviderlistArrayList.clear();
                    EpaLog.e(TAG, "statusCode-- " + statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (statusCode == 200) {

                    try {
                        responseBody = response.body().string();
                        //  EpaLog.e(TAG, " responseBody " + responseBody);
                        Reader reader = new InputStreamReader(Utils.getInputstream(responseBody));

                        ServiceProviderResponseList providerResponseList = gson.fromJson(reader, ServiceProviderResponseList.class);
                        //  EpaLog.e(TAG, " providerResponseList-- " + providerResponseList);

                        serviceproviderlist = providerResponseList.getServiceproviderlist();
                        setValuesToFields(serviceproviderlist);
                        getServiceProviderList(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)));
                        //prepareBottomListViewData(serviceproviderlist);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)), 11f));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                } else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Utils.getDoubleFromString(Utils.LATITUDE), Utils.getDoubleFromString(Utils.LONGITUDE)), 11f));

                    if (statusCode == 404) {
                        Utils.showToast(context, "No providers found!");
                    } else {
                        Utils.showToast(context, "Something went wrong!");
                    }

                    progressDialog.dismiss();
                }

            }
        }.execute(new ServiceProviderRequest(checkProvider().toString(), Utils.LATITUDE, Utils.LONGITUDE, checkBrand(brandName)));
    }

    private void setValuesToFields(List<Serviceproviderlist> serviceproviderlist) {
        mMap.clear();
        userLocation = null;
        for (int i = 0; i < serviceproviderlist.size(); i++) {
            LatLng ll = ll = new LatLng(Utils.getDoubleFromString(serviceproviderlist.get(i).getProviderLat()), Utils.getDoubleFromString(serviceproviderlist.get(i).getProviderLon()));
            BitmapDescriptor bitmapMarker = null;
            String providerType = serviceproviderlist.get(i).getProviderType();
            //    EpaLog.e(TAG, "providerType-- " + providerType);
            switch (serviceproviderlist.get(i).getProviderType()) {
                case Utils.AUTHORIZED_DISTRIBUTOR:

                    bitmapMarker = BitmapDescriptorFactory.fromBitmap(small1);
                    Log.e(TAG, "RED");

                    mMap.addMarker(new MarkerOptions().position(ll).snippet(serviceproviderlist.get(i).getProviderType())
                            .icon(bitmapMarker).title(serviceproviderlist.get(i).getProviderName() + "%" + serviceproviderlist.get(i).getProviderId()));
                    //  EpaLog.e(TAG, "location----- " + ll.toString());
                    //  EpaLog.e(TAG, "ID----- " + serviceproviderlist.get(i).getProviderId());
                    break;
                case Utils.FULL_SERVICE_DEALER:

                    bitmapMarker = BitmapDescriptorFactory.fromBitmap(small2);
                    Log.e(TAG, "DARK GRAY");
                    mMap.addMarker(new MarkerOptions().position(ll).snippet(serviceproviderlist.get(i).getProviderType())
                            .icon(bitmapMarker).title(serviceproviderlist.get(i).getProviderName() + "%" + serviceproviderlist.get(i).getProviderId()));

                    // EpaLog.e(TAG, "location----- " + ll.toString());
                    //  EpaLog.e(TAG, "ID----- " + serviceproviderlist.get(i).getProviderId());
                    break;
                case Utils.OEM_FULL_SERVICE_DEALER:

                    bitmapMarker = BitmapDescriptorFactory.fromBitmap(small3);
                    Log.e(TAG, "LIGHT GRAY");
                    mMap.addMarker(new MarkerOptions().position(ll).snippet(serviceproviderlist.get(i).getProviderType())
                            .icon(bitmapMarker).title(serviceproviderlist.get(i).getProviderName() + "%" + serviceproviderlist.get(i).getProviderId()));
                    //   EpaLog.e(TAG, "location----- " + ll.toString());
                    //   EpaLog.e(TAG, "ID----- " + serviceproviderlist.get(i).getProviderId());
                    break;
//                case Utils.OEM_DEALER:
//
//                    bitmapMarker = BitmapDescriptorFactory.fromBitmap(small4);
//                    Log.e(TAG, "WHITE");
//                    mMap.addMarker(new MarkerOptions().position(ll).snippet(serviceproviderlist.get(i).getProviderType())
//                            .icon(bitmapMarker).title(serviceproviderlist.get(i).getProviderName() + "%" + serviceproviderlist.get(i).getProviderId()));
//                    //EpaLog.e(TAG, "location----- " + ll.toString());
//                    //EpaLog.e(TAG, "ID----- " + serviceproviderlist.get(i).getProviderId());
//                    break;
            }

        }
    }

    private void prepareBottomListViewData(List<Serviceproviderlist> serviceproviderlist) {
        try {
            if (serviceproviderlist != null && serviceproviderlist.size() > 0) {
                for (Serviceproviderlist Object : serviceproviderlist) {

                    Serviceproviderlist bottomListViewData = new Serviceproviderlist(Object.getProviderId(), Object.getProviderName(), Object.getColor(), Object.getProviderLat(), Object.getProviderLon(), Object.getProviderAddress(), Object.getProviderType(), Object.getIs24hourContact());
                    serviceproviderlistArrayList.add(bottomListViewData);
                }
                bottomListViewAdapter = new BottomListViewAdapterNew(serviceproviderlist);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recView.setLayoutManager(mLayoutManager);
                recView.setItemAnimator(new DefaultItemAnimator());
                recView.setAdapter(bottomListViewAdapter);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    private String getProviderId(String title) {
//        String providerId = "";
//        if (serviceproviderlistArrayList != null) {
//            for (Serviceproviderlist responseList : serviceproviderlistArrayList) {
//                EpaLog.e(TAG,"getProviderId---title--"+title);
//                EpaLog.e(TAG,"getProviderId---getProviderName--"+responseList.getProviderName());
//                if (responseList.getProviderName().contains(title)) {
//                    providerId = responseList.getProviderId();
//                    return providerId;
//                }
//            }
//        }
//        return "";
//    }

    private StringBuilder checkProvider() {
        StringBuilder builder = new StringBuilder();
        if (cb_authorized_distributor.isChecked() && cb_full_service_dealer.isChecked() && cb_filter_by_oem.isChecked()) {
            builder.append("1,2,5");
            return builder;
        }

        if (cb_authorized_distributor.isChecked() && cb_full_service_dealer.isChecked()) {
            builder.append("1,2");
            return builder;
        }

        if (cb_authorized_distributor.isChecked() && cb_filter_by_oem.isChecked()) {
            builder.append("1,5");
            return builder;
        }

        if (cb_full_service_dealer.isChecked() && cb_filter_by_oem.isChecked()) {
            builder.append("2,5");
            return builder;
        }

        if (!cb_authorized_distributor.isChecked() && !cb_full_service_dealer.isChecked() && !cb_filter_by_oem.isChecked()) {
            builder.append("All");
            return builder;
        }

        if (cb_authorized_distributor.isChecked()) {
            builder.append("1");
            return builder;
        }

        if (cb_full_service_dealer.isChecked()) {
            builder.append("2");
            return builder;
        }

        if (cb_filter_by_oem.isChecked()) {
            builder.append("5");
            return builder;
        }



        EpaLog.e(TAG, "builder ---- " + builder.toString());

        return null;

    }

    private String checkBrand(String title) {
        if (!cb_filter_by_oem.isChecked()) {
            title = "";
        }

        return title;

    }

    @SuppressLint("StaticFieldLeak")
    private void getServiceProviderList(LatLng latLng) {
        new AsyncTask<ServiceProviderRequest, Void, ServiceProviderResponseList>() {
            Response response;
            int statusCode = 0;
            Gson gson = new Gson();
            String responseBody = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected ServiceProviderResponseList doInBackground(ServiceProviderRequest... serviceProviderRequests) {

                OkHttpClient client = new OkHttpClient();
                Gson gson = new Gson();
                String stringBody = gson.toJson(serviceProviderRequests[0]);
                EpaLog.e(TAG, "stringBody-- " + stringBody);

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, stringBody);
                Request request = new Request.Builder()
                        .url(WebUtils.URL+WebUtils.GETSERVICEPROVIDERSLIST)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .build();

                EpaLog.e(TAG, "ProviderList -- " + WebUtils.GETSERVICEPROVIDERSLIST);

                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ServiceProviderResponseList serviceProviderResponseList) {
                super.onPostExecute(serviceProviderResponseList);

                try {
                    statusCode = response.code();
                    EpaLog.e(TAG, "statusCode-- " + statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (statusCode == 200) {
                    try {
                        responseBody = response.body().string();
                        Reader reader = new InputStreamReader(Utils.getInputstream(responseBody));

                        ServiceProviderResponseList providerResponseList = gson.fromJson(reader, ServiceProviderResponseList.class);
                        EpaLog.e(TAG, " providerResponseList-- " + providerResponseList);

                        serviceproviderlist = providerResponseList.getServiceproviderlist();
                        recView.setVisibility(View.VISIBLE);
                        prepareBottomListViewData(serviceproviderlist);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    // recView.removeAllViewsInLayout(); // remove previous data from Recyclerview
                    tv_no_data.setVisibility(View.VISIBLE);
                    recView.setVisibility(View.GONE);
                }
            }
        }.execute(new ServiceProviderRequest(checkProvider().toString(), latLng.latitude + "", latLng.longitude + "",checkBrand(brandName)));
    }


}