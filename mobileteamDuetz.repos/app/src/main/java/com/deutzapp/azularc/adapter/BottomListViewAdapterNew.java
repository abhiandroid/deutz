package com.deutzapp.azularc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deutzapp.R;
import com.deutzapp.azularc.activities.DetailsActivity;
import com.deutzapp.azularc.data.Serviceproviderlist;
import com.deutzapp.azularc.utils.EpaLog;
import com.deutzapp.azularc.utils.Utils;

import java.util.List;

public class BottomListViewAdapterNew extends
        RecyclerView.Adapter<BottomListViewAdapterNew.PostingViewHolder> {

    private Context context;
    private List<Serviceproviderlist> bottomListViewDataList;

    public BottomListViewAdapterNew(List<Serviceproviderlist> bottomListViewDataList) {
        this.bottomListViewDataList = bottomListViewDataList;
    }

    @Override
    public int getItemCount() {
        return bottomListViewDataList.size();
    }

    @Override
    public void onBindViewHolder(final PostingViewHolder postingViewHolder, final int j) {
        final Serviceproviderlist bottomListViewData = bottomListViewDataList.get(postingViewHolder.getAdapterPosition());

        postingViewHolder.tv_name.setText(bottomListViewData.getProviderName());
        postingViewHolder.tv_type.setText(bottomListViewData.getProviderType());
        postingViewHolder.tv_address.setText(bottomListViewData.getProviderAddress());
        if (bottomListViewData.getColor().equals("Red"))
            postingViewHolder.v_color.setBackgroundColor(Color.RED);

        if (bottomListViewData.getColor().equals("Black"))
            postingViewHolder.v_color.setBackgroundColor(Color.DKGRAY);

        if (bottomListViewData.getColor().equals("Gray"))
            postingViewHolder.v_color.setBackgroundColor(Color.LTGRAY);

        if (bottomListViewData.getColor().equals("darkgrey"))
            postingViewHolder.v_color.setBackgroundColor(Color.DKGRAY);

        if (bottomListViewData.getColor().equals("lightgrey"))
            postingViewHolder.v_color.setBackgroundColor(Color.LTGRAY);



//        if (bottomListViewData.getColor().equals("White")) {
//            postingViewHolder.v_color.setBackgroundColor(Color.LTGRAY);
//            postingViewHolder.v_color.setBackgroundResource(R.drawable.color_border);
//        }

        if (bottomListViewData.getIs24hourContact().equals("1")) {
            postingViewHolder.iv_24_icon.setVisibility(View.VISIBLE);
        }else{
            postingViewHolder.iv_24_icon.setVisibility(View.GONE);
        }

        postingViewHolder.ll_show_item_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = bottomListViewData.getProviderId();
                EpaLog.e("TAG", "id---" + id);
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra(Utils.PROVIDERID, id); // PROVIDER ID
                context.startActivity(intent);
            }
        });

    }

    @Override
    public PostingViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.ll_bottom_listview_content, viewGroup, false);

        this.context = viewGroup.getContext();
        return new PostingViewHolder(itemView);
    }


    static class PostingViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_type;
        private TextView tv_address;
        View v_color;
        ImageView iv_24_icon;
        private LinearLayout ll_show_item_details;

        PostingViewHolder(View v) {
            super(v);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            tv_type = (TextView) v.findViewById(R.id.tv_type);
            tv_address = (TextView) v.findViewById(R.id.tv_address);
            v_color = (View) v.findViewById(R.id.v_color);
            iv_24_icon = (ImageView) v.findViewById(R.id.iv_24_icon);
            ll_show_item_details = (LinearLayout) v.findViewById(R.id.ll_show_item_details);

        }
    }

}
